﻿namespace IReaderSEFAZAM
{
    partial class frmIReaderSEFAZ
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbSoLerNotas = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lbAtividade = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbDtFim = new System.Windows.Forms.Label();
            this.lbDtInicio = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbConnectString = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbNomeProcesso = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.cbSoBaixar = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TbBrowser = new System.Windows.Forms.TabPage();
            this.timerFechar = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cbSoLerNotas);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.lbAtividade);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lbDtFim);
            this.panel1.Controls.Add(this.lbDtInicio);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lbConnectString);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lbNomeProcesso);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lbStatus);
            this.panel1.Controls.Add(this.cbSoBaixar);
            this.panel1.Location = new System.Drawing.Point(12, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(772, 62);
            this.panel1.TabIndex = 9;
            // 
            // cbSoLerNotas
            // 
            this.cbSoLerNotas.AutoSize = true;
            this.cbSoLerNotas.Enabled = false;
            this.cbSoLerNotas.Location = new System.Drawing.Point(292, 43);
            this.cbSoLerNotas.Name = "cbSoLerNotas";
            this.cbSoLerNotas.Size = new System.Drawing.Size(57, 17);
            this.cbSoLerNotas.TabIndex = 15;
            this.cbSoLerNotas.Text = "Só Ler";
            this.cbSoLerNotas.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(355, 34);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(412, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 13;
            // 
            // lbAtividade
            // 
            this.lbAtividade.AutoSize = true;
            this.lbAtividade.Location = new System.Drawing.Point(373, 3);
            this.lbAtividade.Name = "lbAtividade";
            this.lbAtividade.Size = new System.Drawing.Size(67, 13);
            this.lbAtividade.TabIndex = 12;
            this.lbAtividade.Text = "ATIVIDADE:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(303, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "ATIVIDADE:";
            // 
            // lbDtFim
            // 
            this.lbDtFim.AutoSize = true;
            this.lbDtFim.Location = new System.Drawing.Point(70, 47);
            this.lbDtFim.Name = "lbDtFim";
            this.lbDtFim.Size = new System.Drawing.Size(65, 13);
            this.lbDtFim.TabIndex = 10;
            this.lbDtFim.Text = "13/08/2019";
            // 
            // lbDtInicio
            // 
            this.lbDtInicio.AutoSize = true;
            this.lbDtInicio.Location = new System.Drawing.Point(70, 29);
            this.lbDtInicio.Name = "lbDtInicio";
            this.lbDtInicio.Size = new System.Drawing.Size(65, 13);
            this.lbDtInicio.TabIndex = 9;
            this.lbDtInicio.Text = "13/08/2019";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "DTFIM:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "DTINICIO:";
            // 
            // lbConnectString
            // 
            this.lbConnectString.AutoSize = true;
            this.lbConnectString.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnectString.ForeColor = System.Drawing.Color.Green;
            this.lbConnectString.Location = new System.Drawing.Point(73, 1);
            this.lbConnectString.Name = "lbConnectString";
            this.lbConnectString.Size = new System.Drawing.Size(220, 13);
            this.lbConnectString.TabIndex = 6;
            this.lbConnectString.Text = "PALMFASTBISTRO-momo2304-SEFAZAM.pfx";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(14, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Certificado:";
            // 
            // lbNomeProcesso
            // 
            this.lbNomeProcesso.AutoSize = true;
            this.lbNomeProcesso.Location = new System.Drawing.Point(262, 21);
            this.lbNomeProcesso.Name = "lbNomeProcesso";
            this.lbNomeProcesso.Size = new System.Drawing.Size(123, 13);
            this.lbNomeProcesso.TabIndex = 4;
            this.lbNomeProcesso.Text = "IReaderSEFAZAM-XYZ3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(187, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PROCESSO:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "STATUS:";
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Location = new System.Drawing.Point(72, 14);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(52, 13);
            this.lbStatus.TabIndex = 1;
            this.lbStatus.Text = "PARADO";
            // 
            // cbSoBaixar
            // 
            this.cbSoBaixar.AutoSize = true;
            this.cbSoBaixar.Enabled = false;
            this.cbSoBaixar.Location = new System.Drawing.Point(187, 43);
            this.cbSoBaixar.Name = "cbSoBaixar";
            this.cbSoBaixar.Size = new System.Drawing.Size(101, 17);
            this.cbSoBaixar.TabIndex = 14;
            this.cbSoBaixar.Text = "Só Baixar XMLs";
            this.cbSoBaixar.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TbBrowser);
            this.tabControl1.Location = new System.Drawing.Point(12, 77);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 364);
            this.tabControl1.TabIndex = 8;
            // 
            // TbBrowser
            // 
            this.TbBrowser.AutoScroll = true;
            this.TbBrowser.Location = new System.Drawing.Point(4, 22);
            this.TbBrowser.Name = "TbBrowser";
            this.TbBrowser.Padding = new System.Windows.Forms.Padding(3);
            this.TbBrowser.Size = new System.Drawing.Size(768, 338);
            this.TbBrowser.TabIndex = 0;
            this.TbBrowser.Text = "Browser";
            this.TbBrowser.UseVisualStyleBackColor = true;
            // 
            // timerFechar
            // 
            this.timerFechar.Tick += new System.EventHandler(this.timerFechar_Tick);
            // 
            // frmIReaderSEFAZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmIReaderSEFAZAM";
            this.Text = "IReaderSEFAZ Amazonas";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbDtFim;
        private System.Windows.Forms.Label lbDtInicio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbConnectString;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbNomeProcesso;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TbBrowser;
        private System.Windows.Forms.Label lbAtividade;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox cbSoBaixar;
        private System.Windows.Forms.CheckBox cbSoLerNotas;
        private System.Windows.Forms.Timer timerFechar;
    }
}

