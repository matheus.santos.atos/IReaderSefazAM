﻿using AtosCapital;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IReaderSEFAZAM.Classes
{
    public class tbNotaFiscalLog
    {
        public int idNotaFiscalLog { get; set; }
        public string dscertificado { get; set; }
        public DateTime dtInsert { get; set; }
        public DateTime? dtFinalizado { get; set; }
        public DateTime dtInicio { get; set; }
        public DateTime dtFim { get; set; }
        public DateTime dtExecucao { get; set; }
        public DateTime dtExecucaoInicial { get; set; }
        public decimal? vlTotalNotas { get; set; }
        public int? qtInsercoes { get; set; }
        public bool? flSoBaixar { get; set; }
        public bool? flSoLer { get; set; }
        public int idCertificado { get; set; }
        public string nrCNPJ { get; set; }
        public string dsSefaz { get; set; }
    }

}
