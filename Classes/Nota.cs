﻿using AtosCapital;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using static IReaderSEFAZAM.frmIReaderSEFAZ;

namespace IReaderSEFAZAM.Classes
{
    public class Nota
    {
        private List<string> naoautorizadas = new List<string>();
        private List<string> NotasCancelamento = new List<string>();
        private int ArqFormatoDiferente = 0;
        private int notasnaoautorizadas = 0;
        private int ErroLerArquivo = 0;
        private List<string> AqCorrompido = new List<string>();
        private int inseridos = 0, naoinseridos = 0;
        private Dictionary<string, string> cnpjNaoExistente = new Dictionary<string, string>();


        public event EventEscreveArquivoTexto escreveArquivoTexto;
        public event EventEscreveArquivoTextoLista<NotaFiscal> escreveArquivoTextoLista;
        public event EventPreencheArquivoTextoLista<NotaFiscal> preencheArquivoTextoLista;

        #region  Método responsável por criar uma lista com os nomes dos arquivos
        private List<string> getListArquivos(string caminho, string mascara)
        {

            //string mascara = "*.xml";
            // Le todos os arquivos de um tipo no diretorio
            DirectoryInfo arquivos = new DirectoryInfo(caminho);
            // Pega todas as imagens .gif
            System.IO.FileInfo[] fileNames = arquivos.GetFiles(mascara);
            List<string> listName = new List<string>();
            int total = fileNames.Length;
            Thread.Sleep(1);
            foreach (System.IO.FileInfo fi in fileNames)
            {
                listName.Add(fi.Name);
            }

            return listName;
        }
        #endregion

        #region Ler arquivos e armazena as informações na base da Atos
        public string LerArmazenarXML(Dictionary<string, string> dicionarioArquivoTexto, List<NotaFiscal> NotasFiscais, frmIReaderSEFAZ form)
        {
            string msgRetorono = "";

            using (BancoDados conn = new BancoDados(true))
            {
                #region Ler arquivos e armazena as informações na base da Atos
                if (NotasFiscais.Count > 0 || ErroLerArquivo > 0 || NotasCancelamento.Count > 0)
                {
                    try
                    {
                        //tran = conn.BeginTransaction();
                        OpenConnection(dicionarioArquivoTexto, NotasFiscais, conn, form);
                        //tran.Commit();

                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    var listDate = NotasFiscais.GroupBy(tt => new { dtEmissao = tt.dtEmissao.ToString("dd/MM/yyyy"), tt.nrCNPJEmt }).Select(
                        kk => new
                        {
                            vlTotal = kk.Sum(ss => ss.listFormaPgto.Sum(ll => ll.vlPag)),
                            data = kk.Key.dtEmissao,
                            cnpj = kk.Key.nrCNPJEmt

                        }
                        );

                    //AtualizaProgresso(0, 0, " =========================================");
                    StringBuilder arquivo = new StringBuilder();

                    arquivo.AppendLine(string.Join(";", "CNPJ;dtEMISSAO;vlTotal"));
                    foreach (var agr in listDate)
                    {
                        decimal vlTotal = agr.vlTotal;
                        msgRetorono = msgRetorono + "[ vlTotal: R$" + vlTotal + ", dtEmissao: " + agr.data + ", cnpj: " + agr.cnpj + "]";
                        arquivo.AppendLine(string.Join(";", agr.cnpj + ";" + agr.data + "; R$" + vlTotal));

                        Thread.Sleep(1);
                    }

                    //foreach (var agr in listDate)
                    //{
                    //    decimal vlTotal = agr.vlTotal;
                    //    Thread.Sleep(1);
                    //}

                }
                #endregion
            }
            return msgRetorono;
        }
        #endregion

        #region Método responsável por ler os arquivos XML
        public List<NotaFiscal> LerXML(Dictionary<string, string> dicionarioArquivoTexto, List<string> ls, string caminho, frmIReaderSEFAZ form)
        {
            List<NotaFiscal> Lnotas = new List<NotaFiscal>();
            preencheArquivoTextoLista("notas", Lnotas, true);

            naoautorizadas = new List<string>();
            NotasCancelamento = new List<string>();
            ArqFormatoDiferente = 0;
            notasnaoautorizadas = 0;
            ErroLerArquivo = 0;
            AqCorrompido = new List<string>();
            int total = ls.Count - 1;
            int contador = 0;
            Thread.Sleep(1);
            ls = ls.OrderBy(x => x).ToList();
            int startNotaProcessamento = Convert.ToInt32(dicionarioArquivoTexto["lastNotaProcessamento"]);
            for (int l = Convert.ToInt32(dicionarioArquivoTexto["lastLoteProcessamento"]); l < ls.Count; l++)
            {
                string nomedoarquivo = ls[l];
                XmlDocument xd = new XmlDocument();
                string nome = nomedoarquivo;


                try
                {
                    string arq = File.ReadAllText(caminho + @"\" + nome);
                    xd.LoadXml(arq);
                }
                catch (Exception ex)
                {
                    throw new Exception("O arquivo " + nomedoarquivo + " está corrompido!");
                }
                XmlNodeList nodes = xd.DocumentElement.SelectNodes("//*");


                List<XmlNode> Lnodes = new List<XmlNode>();
                XmlNode ide = null;
                XmlNode emit = null;
                XmlNode ICMSTot = null;
                XmlNode infProt = null;
                XmlNode dest = null;
                XmlNode infNFe = null;
                XmlNode infEvento = null;
                XmlNode indSinc = null;
                XmlNode xMotivo = null;
                List<XmlNode> CFeCanc = new List<XmlNode>();
                XmlNode infCFe = null;
                List<XmlNode> det = new List<XmlNode>();
                List<XmlNode> pag = new List<XmlNode>();
                XmlNode pgto = null;
                List<NotaFiscal> ListNotaFiscal = new List<NotaFiscal>();

                foreach (XmlNode node in nodes)
                {
                    Lnodes.Add(node);
                }
                List<XmlNode> CFes = Lnodes.Where(e => e.Name.Equals("CFe")).ToList();
                XmlNode envCFe = Lnodes.Where(e => e.Name.Equals("envCFe")).FirstOrDefault();
                try
                {
                    if (CFes.Count == 0 && envCFe == null)
                    {
                        #region Monta Nota Padrão
                        ide = Lnodes.Where(e => e.Name.Equals("ide")).FirstOrDefault();
                        emit = Lnodes.Where(e => e.Name.Equals("emit")).FirstOrDefault();
                        det = Lnodes.Where(e => e.Name.Equals("det")).ToList();
                        ICMSTot = Lnodes.Where(e => e.Name.Equals("ICMSTot")).FirstOrDefault();
                        pag = Lnodes.Where(e => e.Name.Equals("pag")).ToList();
                        infProt = Lnodes.Where(e => e.Name.Equals("infProt")).FirstOrDefault();
                        dest = Lnodes.Where(e => e.Name.Equals("dest")).FirstOrDefault();
                        infNFe = Lnodes.Where(e => e.Name.Equals("infNFe")).FirstOrDefault();
                        infEvento = Lnodes.Where(e => e.Name.Equals("infEvento")).FirstOrDefault();
                        CFeCanc = Lnodes.Where(e => e.Name.Equals("CFeCanc")).ToList();
                        infCFe = Lnodes.Where(e => e.Name.Equals("infCFe")).FirstOrDefault();
                        indSinc = Lnodes.Where(e => e.Name.Equals("indSinc")).FirstOrDefault();
                        xMotivo = Lnodes.Where(e => e.Name.Equals("xMotivo")).FirstOrDefault();

                        string dtCon = null;
                        if (ide != null)
                            dtCon = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;

                        if (ide != null && (CFeCanc == null || CFeCanc.Count() == 0))// && xMotivo !=null && (xMotivo.InnerText == "Autorizado o uso da NF-e" || dtCon ==null))
                        {
                            string versao = infNFe.OuterXml.Replace("versao=\"", "$").Split('$')[1].Split('"')[0];
                            if ((indSinc != null && versao.Equals("3.10")))
                            {
                                notasnaoautorizadas++;
                                string nrChaveAcesso = infProt != null && infProt["chNFe"].InnerText != "" ? infProt["chNFe"].InnerText : infNFe.OuterXml.Replace("Id=\"NFe", "$").Split('$')[1].Split('"')[0].ToString();
                                naoautorizadas.Add(nrChaveAcesso);
                            }
                            else
                            {
                                try
                                {

                                    NotaFiscal notafiscal = new NotaFiscal();
                                    notafiscal.xml = xd.InnerXml;
                                    notafiscal.XMLname = nome;
                                    //string teste = infNFe.OuterXml.Replace("Id=\"NFe", "$").Split('$')[1].Split('"')[0].ToString();
                                    notafiscal.nrChaveAcesso = infProt != null && infProt["chNFe"].InnerText != "" ? infProt["chNFe"].InnerText : infNFe.OuterXml.Replace("Id=\"NFe", "$").Split('$')[1].Split('"')[0].ToString();

                                    notafiscal.nrProtocolo = infProt != null ? infProt["nProt"].InnerText : "";

                                    notafiscal.cdNota = ide["cNF"].InnerText;
                                    notafiscal.nrModelo = int.Parse(ide["mod"].InnerText);
                                    notafiscal.nrSerie = int.Parse(ide["serie"].InnerText);
                                    notafiscal.nrNota = int.Parse(ide["nNF"].InnerText);
                                    string dhEmi = ide["dhEmi"].InnerText;
                                    string dataEm = dhEmi.Split('T')[0] + " " + dhEmi.Split('T')[1].Split('-')[0];
                                    notafiscal.dtEmissao = DateTime.Parse(dataEm);
                                    string dhCont = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;
                                    string dataCont = dhCont != null ? dhCont.Split('T')[0] + " " + dhCont.Split('T')[1].Split('-')[0] : null;
                                    notafiscal.dtContigencia = dataCont != null ? DateTime.Parse(dataCont) : notafiscal.dtContigencia;
                                    notafiscal.dsProcesso = ide["procEmi"].InnerText;
                                    notafiscal.dsNaturezaOpr = ide["natOp"].InnerText;
                                    notafiscal.xMotivo = xMotivo != null ? xMotivo.InnerText : null;
                                    #region Dados do Emitente
                                    //if ((indSinc != null && versao.Equals("3.10") && !cbstatus.Checked))
                                    //	notafiscal.nrCNPJEmt = "45030279000194";
                                    //else
                                    notafiscal.nrCNPJEmt = emit["CNPJ"].InnerText;
                                    //notafiscal.nrCNPJEmt = "04881349000130";
                                    //notafiscal.nrChaveAcesso = "T" + notafiscal.nrChaveAcesso.Substring(1, 43);
                                    //if (notafiscal.nrCNPJEmt.Equals("24068621000175"))
                                    //{
                                    //	notafiscal.nrCNPJEmt = "04881349000130";
                                    //	notafiscal.nrChaveAcesso = "T" + notafiscal.nrChaveAcesso.Substring(1, 43);
                                    //}
                                    //else if (notafiscal.nrCNPJEmt.Equals("24068621000256"))
                                    //{
                                    //	notafiscal.nrCNPJEmt = "04881349000130";
                                    //}
                                    string xNome = emit["xNome"].InnerText;
                                    string xFant = emit["xFant"].InnerText;
                                    notafiscal.dsRazaoSocialEmt = xNome + " - " + xFant;
                                    #endregion

                                    #region Dados do destinatário
                                    string CPFDest = null;
                                    string CNPJDest = null;
                                    CPFDest = dest != null && dest["CPF"] != null ? dest["CPF"].InnerText : "";
                                    CNPJDest = dest != null && dest["CNPJ"] != null ? dest["CNPJ"].InnerText : "";
                                    notafiscal.nrCnpjCpfDest = CPFDest + "" + CNPJDest;
                                    notafiscal.dsRazaoSocialDest = dest != null && dest["Dest "] != null ? dest["emailDest "].InnerText : "";
                                    if ((indSinc != null && versao.Equals("3.10")))
                                        notafiscal.dsRazaoSocialDest = notafiscal.dsRazaoSocialDest + "C" + emit["CNPJ"].InnerText; ;

                                    #endregion

                                    #region  Obtendo os valores
                                    notafiscal.vlBaseICMS = decimal.Parse(ICMSTot["vBC"].InnerText.Replace('.', ','));
                                    notafiscal.vlICMS = decimal.Parse(ICMSTot["vICMS"].InnerText.Replace('.', ','));
                                    notafiscal.vlICMSDesonerado = decimal.Parse(ICMSTot["vICMSDeson"].InnerText.Replace('.', ','));
                                    notafiscal.vlBaseICMSST = decimal.Parse(ICMSTot["vBCST"].InnerText.Replace('.', ','));
                                    notafiscal.vlICMSSub = decimal.Parse(ICMSTot["vST"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalProdutos = decimal.Parse(ICMSTot["vProd"].InnerText.Replace('.', ','));
                                    notafiscal.vlFrete = decimal.Parse(ICMSTot["vFrete"].InnerText.Replace('.', ','));
                                    notafiscal.vlSeguro = decimal.Parse(ICMSTot["vSeg"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalDesconto = decimal.Parse(ICMSTot["vDesc"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalII = decimal.Parse(ICMSTot["vII"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalIPI = decimal.Parse(ICMSTot["vIPI"].InnerText.Replace('.', ','));
                                    notafiscal.vlPIS = decimal.Parse(ICMSTot["vPIS"].InnerText.Replace('.', ','));
                                    notafiscal.vlCOFINS = decimal.Parse(ICMSTot["vCOFINS"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalNFe = decimal.Parse(ICMSTot["vNF"].InnerText.Replace('.', ','));
                                    notafiscal.vlOutrasDesp = decimal.Parse(ICMSTot["vOutro"].InnerText.Replace('.', ','));
                                    #endregion

                                    #region Obtendo Formas de Pagamento

                                    foreach (XmlNode pg in pag)
                                    {

                                        NotaFiscalFormaPgto formapgto = new NotaFiscalFormaPgto();
                                        if (pg.FirstChild.Name.Equals("detPag"))
                                        {
                                            List<XmlNode> detPag = Lnodes.Where(e => e.Name.Equals("detPag")).ToList();
                                            Thread.Sleep(1);
                                            foreach (XmlNode detPg in detPag)
                                            {
                                                formapgto = new NotaFiscalFormaPgto();
                                                formapgto.cdFormaPgto = int.Parse(detPg["tPag"].InnerText);
                                                formapgto.vlPag = decimal.Parse(detPg["vPag"].InnerText.Replace('.', ','));
                                                formapgto.dsFormaPgto = getFormaPgto(formapgto.cdFormaPgto);
                                                int tBand = pg["tBand"] != null ? int.Parse(pg["tBand"].InnerText) : 0;
                                                formapgto.dsBandeira = getBandeira(tBand);
                                                formapgto.nrAutorizacao = pg["cAut"] != null && pg["cAut"].InnerText != "" ? int.Parse(pg["cAut"].InnerText) : 0;
                                                notafiscal.listFormaPgto.Add(formapgto);
                                            }

                                        }
                                        else
                                        {
                                            formapgto = new NotaFiscalFormaPgto();
                                            if (pg["tPag"] != null)
                                            {
                                                formapgto.cdFormaPgto = int.Parse(pg["tPag"].InnerText);
                                                formapgto.vlPag = decimal.Parse(pg["vPag"].InnerText.Replace('.', ','));
                                            }
                                            else
                                            {
                                                XmlNode detPag = pg["detPag"];
                                                formapgto.cdFormaPgto = int.Parse(detPag["tPag"].InnerText);
                                                formapgto.vlPag = decimal.Parse(detPag["vPag"].InnerText.Replace('.', ','));

                                            }
                                            formapgto.dsFormaPgto = getFormaPgto(formapgto.cdFormaPgto);
                                            int tBand = pg["tBand"] != null ? int.Parse(pg["tBand"].InnerText) : 0;
                                            formapgto.dsBandeira = getBandeira(tBand);
                                            formapgto.nrAutorizacao = pg["cAut"] != null && pg["cAut"].InnerText != "" ? int.Parse(pg["cAut"].InnerText) : 0;
                                            notafiscal.listFormaPgto.Add(formapgto);
                                        }

                                        decimal troco = pg["vTroco"] != null && pg["vTroco"].InnerText != "" ? decimal.Parse(pg["vTroco"].InnerText.Replace('.', ',')) : 0;
                                        Thread.Sleep(1);

                                        int[] formaspgto = { 1, 99, 2 }; //Abate na ordem: Dinheiro, Outros e Cheque
                                        if (troco > 0)
                                        {
                                            if (notafiscal.listFormaPgto.Count == 1)
                                            {
                                                notafiscal.listFormaPgto[0].vlPag -= troco; // Se tiver só um pagamento abate o valor do desconto desse pagamento
                                                troco = 0;
                                            }
                                            else
                                            {
                                                foreach (int p in formaspgto)
                                                {
                                                    for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                                    {
                                                        if (notafiscal.listFormaPgto[i].cdFormaPgto == p)
                                                        {
                                                            if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                            {
                                                                notafiscal.listFormaPgto[i].vlPag -= troco;
                                                                troco = 0;
                                                            }
                                                            else
                                                            {
                                                                troco -= notafiscal.listFormaPgto[i].vlPag;
                                                                notafiscal.listFormaPgto.RemoveAt(i);
                                                                i--;
                                                            }
                                                            if (troco == 0)
                                                                break;
                                                        }
                                                    }
                                                    if (troco == 0)
                                                        break;
                                                }
                                                while (troco > 0) // Se ainda existir saldo eu tiro do maior
                                                {
                                                    notafiscal.listFormaPgto = notafiscal.listFormaPgto.OrderByDescending(x => x.vlPag).ToList();
                                                    for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                                    {
                                                        if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                        {
                                                            notafiscal.listFormaPgto[i].vlPag -= troco;
                                                            troco = 0;
                                                        }
                                                        else
                                                        {
                                                            troco -= notafiscal.listFormaPgto[i].vlPag;
                                                            notafiscal.listFormaPgto.RemoveAt(i);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                    #region Obtendo informações dos itens                                    
                                    foreach (XmlNode item in det)
                                    {
                                        NotaFiscalItem notaitem = new NotaFiscalItem();
                                        XmlNode Prod = item.FirstChild;
                                        XmlNode Imposto = Lnodes.Where(e => e.Name.Equals("imposto")).FirstOrDefault();
                                        notaitem.cdCFOP = int.Parse(Prod["CFOP"].InnerText);
                                        notaitem.cdProduto = Prod["cProd"].InnerText;
                                        notaitem.dsItem = Prod["xProd"].InnerText;
                                        notaitem.dsUnidadeComercial = Prod["uCom"].InnerText;
                                        notaitem.qtItem = decimal.Parse(Prod["qCom"].InnerText.Replace('.', ','));
                                        notaitem.vlUntComercial = decimal.Parse(Prod["vUnCom"].InnerText.Replace('.', ','));
                                        notaitem.vlTotalItem = decimal.Parse(Prod["vProd"].InnerText.Replace('.', ','));
                                        notaitem.cdNCM = int.Parse(Prod["NCM"].InnerText);

                                        string cdEANTributavel = Prod["cEANTrib"] != null && Prod["cEANTrib"].InnerText != "" && !Prod["cEANTrib"].InnerText.Equals("SEM GTIN") ? Prod["cEANTrib"].InnerText : "0";
                                        if (cdEANTributavel.Length > 10)
                                        {
                                            cdEANTributavel = cdEANTributavel.Substring(0, 8);
                                        }
                                        string cdEANComercial = Prod["cEAN"] != null && Prod["cEAN"].InnerText != "" && !Prod["cEAN"].InnerText.Equals("SEM GTIN") ? Prod["cEAN"].InnerText : "0";
                                        if (cdEANComercial.Length > 10)
                                        {
                                            int length = cdEANComercial.Length - 8;
                                            cdEANComercial = cdEANComercial.Substring(8, length);
                                        }
                                        notaitem.cdEANTributavel = int.Parse(cdEANTributavel);
                                        notaitem.cdEANComercial = int.Parse(cdEANComercial);
                                        notaitem.vlUntTributacao = decimal.Parse(Prod["vUnTrib"].InnerText.Replace('.', ','));
                                        notaitem.nrItem = int.Parse(Prod["indTot"].InnerText);



                                        if (Imposto.FirstChild.Name.Equals("ICMS"))
                                        {
                                            #region IMPOSTO
                                            XmlNode ICMS = Imposto.ChildNodes[0].FirstChild;
                                            if (ICMS != null)
                                            {
                                                XmlNode PIS = Imposto.ChildNodes[1].FirstChild;
                                                XmlNode COFINS = Imposto.ChildNodes[2].FirstChild;

                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                                notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                                notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                                notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                                #endregion

                                                #region PIS
                                                notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                                notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion

                                                #region COFINS
                                                notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                                notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = "";
                                                notaitem.vlICMS_Base = 0;
                                                notaitem.cdICMS_Mod = "";
                                                notaitem.cdICMS_Trib = "";
                                                notaitem.vlICMS = 0;
                                                notaitem.vlICMS_Aliq = 0;
                                                #endregion

                                                #region PIS
                                                notaitem.cdPIS_CST = "";
                                                notaitem.vlPIS = 0;
                                                notaitem.vlPIS_Aliq = 0;
                                                notaitem.vlPIS_Base = 0;
                                                #endregion

                                                #region COFINS
                                                notaitem.cdCOFINS_CST = "";
                                                notaitem.vlCOFINS = 0;
                                                notaitem.vlCOFINS_Aliq = 0;
                                                notaitem.vlCOFINS_Base = 0;
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {

                                            #region IMPOSTO
                                            XmlNode ICMS = Imposto.ChildNodes[1].FirstChild;
                                            if (ICMS != null)
                                            {
                                                XmlNode PIS = Imposto.ChildNodes[2].FirstChild;
                                                XmlNode COFINS = Imposto.ChildNodes[3].FirstChild;

                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                                notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                                notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                                notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                                #endregion

                                                #region PIS
                                                notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                                notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion

                                                #region COFINS
                                                notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                                notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = "";
                                                notaitem.vlICMS_Base = 0;
                                                notaitem.cdICMS_Mod = "";
                                                notaitem.cdICMS_Trib = "";
                                                notaitem.vlICMS = 0;
                                                notaitem.vlICMS_Aliq = 0;
                                                #endregion

                                                #region PIS
                                                notaitem.cdPIS_CST = "";
                                                notaitem.vlPIS = 0;
                                                notaitem.vlPIS_Aliq = 0;
                                                notaitem.vlPIS_Base = 0;
                                                #endregion

                                                #region COFINS
                                                notaitem.cdCOFINS_CST = "";
                                                notaitem.vlCOFINS = 0;
                                                notaitem.vlCOFINS_Aliq = 0;
                                                notaitem.vlCOFINS_Base = 0;
                                                #endregion
                                            }
                                            #endregion
                                        }

                                        notafiscal.listItens.Add(notaitem);
                                    }
                                    #endregion

                                    Lnotas.Add(notafiscal);

                                    escreveArquivoTextoLista("notas", Lnotas, Lnotas.Count - 1, true);
                                    //AtualizaProgresso(contador++, total, " Obtendo informação da nota " + notafiscal.nrChaveAcesso);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Falha ao ler o XML " + nomedoarquivo + " => " + ex.Message);
                                    //ErroLerArquivo++;
                                }
                            }


                        }
                        else if (infEvento != null || (CFeCanc != null && CFeCanc.Count > 0))
                        { //Verifica se é uma nota de cancelamento

                            string chNFe = "";
                            if (infEvento != null)
                                chNFe = infEvento["chNFe"] != null ? infEvento["chNFe"].InnerText : "";
                            else if (CFeCanc != null && CFeCanc.Count > 0)
                            {
                                List<XmlNode> infCFes = Lnodes.Where(e => e.Name.Equals("infCFe")).ToList();
                                foreach (XmlNode infCFee in infCFes)
                                {
                                    chNFe = infCFee != null ? infCFee.OuterXml.Replace("chCanc=\"CFe", "$").Split('$')[1].Split('"')[0].ToString() : "";
                                    if (!chNFe.Equals(""))
                                        NotasCancelamento.Add(chNFe);
                                    chNFe = "";
                                }
                                //chNFe = infCFe != null ? infCFe.OuterXml.Replace("chCanc=\"CFe", "$").Split('$')[1].Split('"')[0].ToString() : "";
                            }

                            if (!chNFe.Equals(""))
                                NotasCancelamento.Add(chNFe);
                            else if (CFeCanc == null || CFeCanc.Count == 0)
                            {
                                throw new Exception("Arquivo em formato diferente " + nomedoarquivo);
                            }
                        }
                        else
                        {
                            throw new Exception("Arquivo em formato diferente " + nomedoarquivo);
                        }
                        #endregion
                    }
                    else if (CFes.Count == 0 && envCFe != null)
                    {
                        #region Monta 

                        infNFe = Lnodes.Where(e => e.Name.Equals("infCFe")).FirstOrDefault();
                        ide = Lnodes.Where(e => e.Name.Equals("ide")).FirstOrDefault();
                        emit = Lnodes.Where(e => e.Name.Equals("emit")).FirstOrDefault();
                        det = Lnodes.Where(e => e.Name.Equals("det")).ToList();
                        ICMSTot = Lnodes.Where(e => e.Name.Equals("ICMSTot")).FirstOrDefault();
                        pgto = Lnodes.Where(e => e.Name.Equals("pgto")).First();
                        dest = Lnodes.Where(e => e.Name.Equals("dest")).FirstOrDefault();


                        string dtCon = null;
                        if (ide != null)
                            dtCon = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;

                        if (ide != null)
                        {

                            try
                            {

                                NotaFiscal notafiscal = new NotaFiscal();
                                notafiscal.xml = xd.InnerXml;
                                notafiscal.XMLname = nome;
                                notafiscal.nrChaveAcesso = infProt != null && infProt["chNFe"].InnerText != "" ? infProt["chNFe"].InnerText : infNFe.OuterXml.Replace("Id=\"CFe", "$").Split('$')[1].Split('"')[0].ToString();


                                notafiscal.cdNota = ide["cNF"].InnerText;
                                notafiscal.nrModelo = int.Parse(ide["mod"].InnerText);
                                notafiscal.nrSerie = int.Parse(ide["nserieSAT"].InnerText);
                                notafiscal.nrNota = int.Parse(ide["nCFe"].InnerText);
                                string dhEmi = ide["dEmi"].InnerText;
                                string hEmi = ide["hEmi"].InnerText;
                                string dataEm = dhEmi + " " + hEmi;
                                notafiscal.dtEmissao = DateTime.ParseExact(dataEm, "yyyyMMdd HHmmss", CultureInfo.InvariantCulture);
                                string dhCont = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;
                                notafiscal.dtContigencia = dhCont != null ? DateTime.ParseExact(dhCont, "yyyyMMdd", CultureInfo.InvariantCulture) : notafiscal.dtContigencia;

                                #region Dados do Emitente

                                notafiscal.nrCNPJEmt = emit["CNPJ"].InnerText;

                                string xNome = emit["xNome"].InnerText;
                                string xFant = emit["xFant"] != null ? emit["xFant"].InnerText : null;
                                notafiscal.dsRazaoSocialEmt = xNome + (xFant != null ? " - " + xFant : "");
                                #endregion

                                #region Dados do destinatário
                                string CPFDest = null;
                                string CNPJDest = null;
                                CPFDest = dest != null && dest["CPF"] != null ? dest["CPF"].InnerText : "";
                                CNPJDest = dest != null && dest["CNPJ"] != null ? dest["CNPJ"].InnerText : "";
                                notafiscal.nrCnpjCpfDest = CPFDest + "" + CNPJDest;
                                notafiscal.dsRazaoSocialDest = dest != null && dest["emailDest "] != null ? dest["emailDest "].InnerText : "";
                                #endregion

                                #region  Obtendo os valores
                                notafiscal.vlICMS = decimal.Parse(ICMSTot["vICMS"].InnerText.Replace('.', ','));
                                notafiscal.vlTotalProdutos = decimal.Parse(ICMSTot["vProd"].InnerText.Replace('.', ','));
                                notafiscal.vlTotalDesconto = decimal.Parse(ICMSTot["vDesc"].InnerText.Replace('.', ','));
                                notafiscal.vlPIS = decimal.Parse(ICMSTot["vPIS"].InnerText.Replace('.', ','));
                                notafiscal.vlCOFINS = decimal.Parse(ICMSTot["vCOFINS"].InnerText.Replace('.', ','));
                                notafiscal.vlTotalNFe = decimal.Parse(Lnodes.Where(e => e.Name.Equals("vCFe")).FirstOrDefault().InnerText.Replace('.', ','));
                                notafiscal.vlOutrasDesp = decimal.Parse(ICMSTot["vOutro"].InnerText.Replace('.', ','));
                                #endregion

                                #region Obtendo Formas de Pagamento
                                Thread.Sleep(1);

                                List<XmlNode> MPs = new List<XmlNode>(pgto.ChildNodes.Cast<XmlNode>())
                                    .Where(tt => tt.Name.Equals("MP")).ToList();
                                string vTroco = new List<XmlNode>(pgto.ChildNodes.Cast<XmlNode>())
                                    .Where(tt => tt.Name.Equals("vTroco")).First().InnerText;

                                foreach (XmlNode MP in MPs)
                                {
                                    NotaFiscalFormaPgto formapgto = new NotaFiscalFormaPgto();
                                    formapgto = new NotaFiscalFormaPgto();
                                    formapgto.cdFormaPgto = int.Parse(MP["cMP"].InnerText);
                                    formapgto.vlPag = decimal.Parse(MP["vMP"].InnerText.Replace('.', ','));
                                    formapgto.dsFormaPgto = getFormaPgto(formapgto.cdFormaPgto);
                                    int tBand = MP["tBand"] != null ? int.Parse(MP["tBand"].InnerText) : 0;
                                    formapgto.dsBandeira = getBandeira(tBand);
                                    formapgto.nrAutorizacao = MP["cAut"] != null && MP["cAut"].InnerText != "" ? int.Parse(MP["cAut"].InnerText) : 0;
                                    notafiscal.listFormaPgto.Add(formapgto);

                                }
                                decimal troco = decimal.Parse(vTroco.Replace('.', ','));
                                Thread.Sleep(1);
                                int[] formaspgto = { 1, 99, 2 }; //Abate na ordem: Dinheiro, Outros e Cheque
                                if (troco > 0)
                                {
                                    if (notafiscal.listFormaPgto.Count == 1)
                                    {
                                        notafiscal.listFormaPgto[0].vlPag -= troco; // Se tiver só um pagamento abate o valor do desconto desse pagamento
                                        troco = 0;
                                    }
                                    else
                                    {
                                        foreach (int p in formaspgto)
                                        {
                                            for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                            {
                                                if (notafiscal.listFormaPgto[i].cdFormaPgto == p)
                                                {
                                                    if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                    {
                                                        notafiscal.listFormaPgto[i].vlPag -= troco;
                                                        troco = 0;
                                                    }
                                                    else
                                                    {
                                                        troco -= notafiscal.listFormaPgto[i].vlPag;
                                                        notafiscal.listFormaPgto.RemoveAt(i);
                                                        i--;
                                                    }
                                                    if (troco == 0)
                                                        break;
                                                }
                                            }
                                            if (troco == 0)
                                                break;
                                        }
                                        while (troco > 0) // Se ainda existir saldo eu tiro do maior
                                        {
                                            notafiscal.listFormaPgto = notafiscal.listFormaPgto.OrderByDescending(x => x.vlPag).ToList();
                                            for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                            {
                                                if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                {
                                                    notafiscal.listFormaPgto[i].vlPag -= troco;
                                                    troco = 0;
                                                }
                                                else
                                                {
                                                    troco -= notafiscal.listFormaPgto[i].vlPag;
                                                    notafiscal.listFormaPgto.RemoveAt(i);
                                                }
                                            }
                                        }
                                    }
                                }


                                #endregion

                                #region Obtendo informações dos itens
                                Thread.Sleep(1);
                                foreach (XmlNode item in det)
                                {
                                    NotaFiscalItem notaitem = new NotaFiscalItem();
                                    XmlNode Prod = item.FirstChild;
                                    XmlNode Imposto = Lnodes.Where(e => e.Name.Equals("imposto")).FirstOrDefault();
                                    notaitem.cdCFOP = int.Parse(Prod["CFOP"].InnerText);
                                    notaitem.cdProduto = Prod["cProd"].InnerText;
                                    notaitem.dsItem = Prod["xProd"].InnerText;
                                    notaitem.dsUnidadeComercial = Prod["uCom"].InnerText;
                                    notaitem.qtItem = decimal.Parse(Prod["qCom"].InnerText.Replace('.', ','));
                                    notaitem.vlUntComercial = decimal.Parse(Prod["vUnCom"].InnerText.Replace('.', ','));
                                    notaitem.vlTotalItem = decimal.Parse(Prod["vProd"].InnerText.Replace('.', ','));
                                    notaitem.cdNCM = Prod["NCM"] != null && Prod["NCM"].InnerText != null ? int.Parse(Prod["NCM"].InnerText) : 0;

                                    string cdEANTributavel = Prod["cEANTrib"] != null && Prod["cEANTrib"].InnerText != "" && !Prod["cEANTrib"].InnerText.Equals("SEM GTIN") ? Prod["cEANTrib"].InnerText : "0";
                                    if (cdEANTributavel.Length > 10)
                                    {
                                        cdEANTributavel = cdEANTributavel.Substring(0, 8);
                                    }
                                    string cdEANComercial = Prod["cEAN"] != null && Prod["cEAN"].InnerText != "" && !Prod["cEAN"].InnerText.Equals("SEM GTIN") ? Prod["cEAN"].InnerText : "0";
                                    if (cdEANComercial.Length > 10)
                                    {
                                        int length = cdEANComercial.Length - 8;
                                        cdEANComercial = cdEANComercial.Substring(8, length);
                                    }
                                    notaitem.cdEANTributavel = int.Parse(cdEANTributavel);
                                    notaitem.cdEANComercial = int.Parse(cdEANComercial);
                                    notaitem.vlUntTributacao = Prod["vUnTrib"] != null ? decimal.Parse(Prod["vUnTrib"].InnerText.Replace('.', ',')) : 0;
                                    notaitem.nrItem = int.Parse(item.Attributes[0].InnerText);



                                    if (Imposto.FirstChild.Name.Equals("ICMS"))
                                    {
                                        #region IMPOSTO
                                        XmlNode ICMS = Imposto["ICMS"];
                                        XmlNode PIS = Imposto["PIS"];
                                        XmlNode COFINS = Imposto["COFINS"];

                                        if (ICMS != null)
                                        {
                                            XmlNode ICMSAliq = ICMS["ICMSAliq"];

                                            if (ICMSAliq != null)
                                                ICMS = ICMSAliq;
                                            #region ICMS
                                            notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                            notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                            notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                            notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region ICMS
                                            notaitem.cdICMS_OrigMerc = "";
                                            notaitem.vlICMS_Base = 0;
                                            notaitem.cdICMS_Mod = "";
                                            notaitem.cdICMS_Trib = "";
                                            notaitem.vlICMS = 0;
                                            notaitem.vlICMS_Aliq = 0;
                                            #endregion
                                        }
                                        if (PIS != null)
                                        {
                                            XmlNode PISAliq = PIS["PISAliq"];

                                            if (PISAliq != null)
                                                PIS = PISAliq;
                                            #region PIS
                                            notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                            notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region PIS
                                            notaitem.cdPIS_CST = "";
                                            notaitem.vlPIS = 0;
                                            notaitem.vlPIS_Aliq = 0;
                                            notaitem.vlPIS_Base = 0;
                                            #endregion
                                        }

                                        if (COFINS != null)
                                        {
                                            XmlNode COFINSAliq = COFINS["COFINSAliq"];

                                            if (COFINSAliq != null)
                                                COFINS = COFINSAliq;
                                            #region COFINS
                                            notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                            notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region COFINS
                                            notaitem.cdCOFINS_CST = "";
                                            notaitem.vlCOFINS = 0;
                                            notaitem.vlCOFINS_Aliq = 0;
                                            notaitem.vlCOFINS_Base = 0;
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    else
                                    {

                                        #region IMPOSTO
                                        XmlNode ICMS = Imposto["ICMS"];
                                        XmlNode PIS = Imposto["PIS"];
                                        XmlNode COFINS = Imposto["COFINS"];

                                        if (ICMS != null)
                                        {
                                            XmlNode ICMSAliq = ICMS["ICMSAliq"];

                                            if (ICMSAliq != null)
                                                ICMS = ICMSAliq;
                                            #region ICMS
                                            notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                            notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                            notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                            notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region ICMS
                                            notaitem.cdICMS_OrigMerc = "";
                                            notaitem.vlICMS_Base = 0;
                                            notaitem.cdICMS_Mod = "";
                                            notaitem.cdICMS_Trib = "";
                                            notaitem.vlICMS = 0;
                                            notaitem.vlICMS_Aliq = 0;
                                            #endregion
                                        }
                                        if (PIS != null)
                                        {
                                            XmlNode PISAliq = PIS["PISAliq"];

                                            if (PISAliq != null)
                                                PIS = PISAliq;

                                            #region PIS
                                            notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                            notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region PIS
                                            notaitem.cdPIS_CST = "";
                                            notaitem.vlPIS = 0;
                                            notaitem.vlPIS_Aliq = 0;
                                            notaitem.vlPIS_Base = 0;
                                            #endregion
                                        }

                                        if (COFINS != null)
                                        {
                                            XmlNode COFINSAliq = COFINS["COFINSAliq"];

                                            if (COFINSAliq != null)
                                                COFINS = COFINSAliq;
                                            #region COFINS
                                            notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                            notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                            notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                            #endregion
                                        }
                                        else
                                        {
                                            #region COFINS
                                            notaitem.cdCOFINS_CST = "";
                                            notaitem.vlCOFINS = 0;
                                            notaitem.vlCOFINS_Aliq = 0;
                                            notaitem.vlCOFINS_Base = 0;
                                            #endregion
                                        }

                                        #endregion
                                    }

                                    notafiscal.listItens.Add(notaitem);
                                }
                                #endregion

                                Lnotas.Add(notafiscal);

                                escreveArquivoTextoLista("notas", Lnotas, Lnotas.Count - 1, true);
                                //AtualizaProgresso(contador++, total, " Obtendo informação da nota " + notafiscal.nrChaveAcesso);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Falha ao ler o XML " + nomedoarquivo + " => " + ex.Message);
                                //ErroLerArquivo++;
                            }



                        }
                        else if (infEvento != null)
                        { //Verifica se é uma nota de cancelamento
                            string chNFe = infEvento["chNFe"] != null ? infEvento["chNFe"].InnerText : "";
                            if (!chNFe.Equals(""))
                                NotasCancelamento.Add(chNFe);
                            else
                                ArqFormatoDiferente++;
                        }
                        else
                        {
                            ArqFormatoDiferente++;
                        }
                        #endregion
                    }
                    else
                    {
                        total = CFes.Count;
                        CFes = CFes.OrderBy(x => x.InnerText).ToList(); // ANALISAR SE Name se repete ou não => encontrar ordem única
                        for (int xx = startNotaProcessamento; xx < CFes.Count; xx++)
                        {
                            XmlNode CFe = CFes[xx];
                            #region Monta Nota LOTE

                            string inicio = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> <envCFe versao=\"0.07\" xmlns=\"http://www.fazenda.sp.gov.br/sat\"> ";
                            string fim = " </envCFe>";
                            xd.LoadXml(inicio + CFe.InnerXml.ToString() + fim);
                            nodes = xd.DocumentElement.SelectNodes("//*");
                            Lnodes = new List<XmlNode>();
                            foreach (XmlNode node in nodes)
                            {
                                Lnodes.Add(node);
                            }

                            infNFe = Lnodes.Where(e => e.Name.Equals("infCFe")).FirstOrDefault();
                            ide = Lnodes.Where(e => e.Name.Equals("ide")).FirstOrDefault();
                            emit = Lnodes.Where(e => e.Name.Equals("emit")).FirstOrDefault();
                            det = Lnodes.Where(e => e.Name.Equals("det")).ToList();
                            ICMSTot = Lnodes.Where(e => e.Name.Equals("ICMSTot")).FirstOrDefault();
                            pgto = Lnodes.Where(e => e.Name.Equals("pgto")).First();
                            dest = Lnodes.Where(e => e.Name.Equals("dest")).FirstOrDefault();


                            string dtCon = null;
                            if (ide != null)
                                dtCon = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;

                            if (ide != null)
                            {

                                try
                                {

                                    NotaFiscal notafiscal = new NotaFiscal();
                                    notafiscal.xml = xd.InnerXml;
                                    notafiscal.XMLname = nome;
                                    notafiscal.nrChaveAcesso = infProt != null && infProt["chNFe"].InnerText != "" ? infProt["chNFe"].InnerText : infNFe.OuterXml.Replace("Id=\"CFe", "$").Split('$')[1].Split('"')[0].ToString();


                                    notafiscal.cdNota = ide["cNF"].InnerText;
                                    notafiscal.nrModelo = int.Parse(ide["mod"].InnerText);
                                    notafiscal.nrSerie = int.Parse(ide["nserieSAT"].InnerText);
                                    notafiscal.nrNota = int.Parse(ide["nCFe"].InnerText);
                                    string dhEmi = ide["dEmi"].InnerText;
                                    string hEmi = ide["hEmi"].InnerText;
                                    string dataEm = dhEmi + " " + hEmi;
                                    notafiscal.dtEmissao = DateTime.ParseExact(dataEm, "yyyyMMdd HHmmss", CultureInfo.InvariantCulture);
                                    string dhCont = ide["dhCont"] != null ? ide["dhCont"].InnerText : null;
                                    notafiscal.dtContigencia = dhCont != null ? DateTime.ParseExact(dhCont, "yyyyMMdd", CultureInfo.InvariantCulture) : notafiscal.dtContigencia;

                                    #region Dados do Emitente

                                    notafiscal.nrCNPJEmt = emit["CNPJ"].InnerText;

                                    string xNome = emit["xNome"].InnerText;
                                    string xFant = emit["xFant"] != null ? emit["xFant"].InnerText : null;
                                    notafiscal.dsRazaoSocialEmt = xNome + (xFant != null ? " - " + xFant : "");
                                    #endregion

                                    #region Dados do destinatário
                                    string CPFDest = null;
                                    string CNPJDest = null;
                                    CPFDest = dest != null && dest["CPF"] != null ? dest["CPF"].InnerText : "";
                                    CNPJDest = dest != null && dest["CNPJ"] != null ? dest["CNPJ"].InnerText : "";
                                    notafiscal.nrCnpjCpfDest = CPFDest + "" + CNPJDest;
                                    notafiscal.dsRazaoSocialDest = dest != null && dest["emailDest "] != null ? dest["emailDest "].InnerText : "";
                                    #endregion

                                    #region  Obtendo os valores
                                    notafiscal.vlICMS = decimal.Parse(ICMSTot["vICMS"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalProdutos = decimal.Parse(ICMSTot["vProd"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalDesconto = decimal.Parse(ICMSTot["vDesc"].InnerText.Replace('.', ','));
                                    notafiscal.vlPIS = decimal.Parse(ICMSTot["vPIS"].InnerText.Replace('.', ','));
                                    notafiscal.vlCOFINS = decimal.Parse(ICMSTot["vCOFINS"].InnerText.Replace('.', ','));
                                    notafiscal.vlTotalNFe = decimal.Parse(Lnodes.Where(e => e.Name.Equals("vCFe")).FirstOrDefault().InnerText.Replace('.', ','));
                                    notafiscal.vlOutrasDesp = decimal.Parse(ICMSTot["vOutro"].InnerText.Replace('.', ','));
                                    #endregion

                                    #region Obtendo Formas de Pagamento
                                    Thread.Sleep(1);

                                    List<XmlNode> MPs = new List<XmlNode>(pgto.ChildNodes.Cast<XmlNode>())
                                        .Where(tt => tt.Name.Equals("MP")).ToList();
                                    string vTroco = new List<XmlNode>(pgto.ChildNodes.Cast<XmlNode>())
                                        .Where(tt => tt.Name.Equals("vTroco")).First().InnerText;

                                    foreach (XmlNode MP in MPs)
                                    {
                                        NotaFiscalFormaPgto formapgto = new NotaFiscalFormaPgto();
                                        formapgto = new NotaFiscalFormaPgto();
                                        formapgto.cdFormaPgto = int.Parse(MP["cMP"].InnerText);
                                        formapgto.vlPag = decimal.Parse(MP["vMP"].InnerText.Replace('.', ','));
                                        formapgto.dsFormaPgto = getFormaPgto(formapgto.cdFormaPgto);
                                        int tBand = MP["tBand"] != null ? int.Parse(MP["tBand"].InnerText) : 0;
                                        formapgto.dsBandeira = getBandeira(tBand);
                                        formapgto.nrAutorizacao = MP["cAut"] != null && MP["cAut"].InnerText != "" ? int.Parse(MP["cAut"].InnerText) : 0;
                                        notafiscal.listFormaPgto.Add(formapgto);

                                    }
                                    decimal troco = decimal.Parse(vTroco.Replace('.', ','));

                                    int[] formaspgto = { 1, 99, 2 }; //Abate na ordem: Dinheiro, Outros e Cheque
                                    if (troco > 0)
                                    {
                                        if (notafiscal.listFormaPgto.Count == 1)
                                        {
                                            notafiscal.listFormaPgto[0].vlPag -= troco; // Se tiver só um pagamento abate o valor do desconto desse pagamento
                                            troco = 0;
                                        }
                                        else
                                        {
                                            foreach (int p in formaspgto)
                                            {
                                                for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                                {
                                                    if (notafiscal.listFormaPgto[i].cdFormaPgto == p)
                                                    {
                                                        if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                        {
                                                            notafiscal.listFormaPgto[i].vlPag -= troco;
                                                            troco = 0;
                                                        }
                                                        else
                                                        {
                                                            troco -= notafiscal.listFormaPgto[i].vlPag;
                                                            notafiscal.listFormaPgto.RemoveAt(i);
                                                            i--;
                                                        }
                                                        if (troco == 0)
                                                            break;
                                                    }
                                                }
                                                if (troco == 0)
                                                    break;
                                            }
                                            while (troco > 0) // Se ainda existir saldo eu tiro do maior
                                            {
                                                notafiscal.listFormaPgto = notafiscal.listFormaPgto.OrderByDescending(x => x.vlPag).ToList();
                                                for (int i = 0; i < notafiscal.listFormaPgto.Count; i++)
                                                {
                                                    if (notafiscal.listFormaPgto[i].vlPag > troco)
                                                    {
                                                        notafiscal.listFormaPgto[i].vlPag -= troco;
                                                        troco = 0;
                                                    }
                                                    else
                                                    {
                                                        troco -= notafiscal.listFormaPgto[i].vlPag;
                                                        notafiscal.listFormaPgto.RemoveAt(i);
                                                    }
                                                }
                                            }
                                        }
                                    }


                                    #endregion

                                    #region Obtendo informações dos itens

                                    foreach (XmlNode item in det)
                                    {
                                        NotaFiscalItem notaitem = new NotaFiscalItem();
                                        XmlNode Prod = item.FirstChild;
                                        XmlNode Imposto = Lnodes.Where(e => e.Name.Equals("imposto")).FirstOrDefault();
                                        notaitem.cdCFOP = int.Parse(Prod["CFOP"].InnerText);
                                        notaitem.cdProduto = Prod["cProd"].InnerText;
                                        notaitem.dsItem = Prod["xProd"].InnerText;
                                        notaitem.dsUnidadeComercial = Prod["uCom"].InnerText;
                                        notaitem.qtItem = decimal.Parse(Prod["qCom"].InnerText.Replace('.', ','));
                                        notaitem.vlUntComercial = decimal.Parse(Prod["vUnCom"].InnerText.Replace('.', ','));
                                        notaitem.vlTotalItem = decimal.Parse(Prod["vProd"].InnerText.Replace('.', ','));
                                        notaitem.cdNCM = Prod["NCM"] != null && Prod["NCM"].InnerText != null ? int.Parse(Prod["NCM"].InnerText) : 0;

                                        string cdEANTributavel = Prod["cEANTrib"] != null && Prod["cEANTrib"].InnerText != "" && !Prod["cEANTrib"].InnerText.Equals("SEM GTIN") ? Prod["cEANTrib"].InnerText : "0";
                                        if (cdEANTributavel.Length > 10)
                                        {
                                            cdEANTributavel = cdEANTributavel.Substring(0, 8);
                                        }
                                        string cdEANComercial = Prod["cEAN"] != null && Prod["cEAN"].InnerText != "" && !Prod["cEAN"].InnerText.Equals("SEM GTIN") ? Prod["cEAN"].InnerText : "0";
                                        if (cdEANComercial.Length > 10)
                                        {
                                            int length = cdEANComercial.Length - 8;
                                            cdEANComercial = cdEANComercial.Substring(8, length);
                                        }
                                        notaitem.cdEANTributavel = int.Parse(cdEANTributavel);
                                        notaitem.cdEANComercial = int.Parse(cdEANComercial);
                                        notaitem.vlUntTributacao = Prod["vUnTrib"] != null ? decimal.Parse(Prod["vUnTrib"].InnerText.Replace('.', ',')) : 0;
                                        notaitem.nrItem = int.Parse(item.Attributes[0].InnerText);



                                        if (Imposto.FirstChild.Name.Equals("ICMS"))
                                        {
                                            #region IMPOSTO
                                            XmlNode ICMS = Imposto["ICMS"];
                                            XmlNode PIS = Imposto["PIS"];
                                            XmlNode COFINS = Imposto["COFINS"];

                                            if (ICMS != null)
                                            {
                                                XmlNode ICMSAliq = ICMS["ICMSAliq"];

                                                if (ICMSAliq != null)
                                                    ICMS = ICMSAliq;
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                                notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                                notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                                notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = "";
                                                notaitem.vlICMS_Base = 0;
                                                notaitem.cdICMS_Mod = "";
                                                notaitem.cdICMS_Trib = "";
                                                notaitem.vlICMS = 0;
                                                notaitem.vlICMS_Aliq = 0;
                                                #endregion
                                            }
                                            if (PIS != null)
                                            {
                                                XmlNode PISAliq = PIS["PISAliq"];

                                                if (PISAliq != null)
                                                    PIS = PISAliq;
                                                #region PIS
                                                notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                                notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region PIS
                                                notaitem.cdPIS_CST = "";
                                                notaitem.vlPIS = 0;
                                                notaitem.vlPIS_Aliq = 0;
                                                notaitem.vlPIS_Base = 0;
                                                #endregion
                                            }
                                            if (COFINS != null)
                                            {
                                                XmlNode COFINSAliq = COFINS["COFINSAliq"];

                                                if (COFINSAliq != null)
                                                    COFINS = COFINSAliq;
                                                #region COFINS
                                                notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                                notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region COFINS
                                                notaitem.cdCOFINS_CST = "";
                                                notaitem.vlCOFINS = 0;
                                                notaitem.vlCOFINS_Aliq = 0;
                                                notaitem.vlCOFINS_Base = 0;
                                                #endregion
                                            }

                                            #endregion
                                        }
                                        else
                                        {

                                            #region IMPOSTO
                                            XmlNode ICMS = Imposto["ICMS"];
                                            XmlNode PIS = Imposto["PIS"];
                                            XmlNode COFINS = Imposto["COFINS"];

                                            if (ICMS != null)
                                            {
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = ICMS["orig"] != null ? ICMS["orig"].InnerText : "";
                                                notaitem.vlICMS_Base = ICMS["vBC"] != null && ICMS["vBC"].InnerText != "" ? decimal.Parse(ICMS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.cdICMS_Mod = ICMS["modBC"] != null ? ICMS["modBC"].InnerText : "";
                                                notaitem.cdICMS_Trib = ICMS["CST"] != null ? ICMS["CST"].InnerText : "";
                                                notaitem.vlICMS = ICMS["vICMS"] != null && ICMS["vICMS"].InnerText != "" ? decimal.Parse(ICMS["vICMS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlICMS_Aliq = ICMS["pICMS"] != null && ICMS["pICMS"].InnerText != "" ? decimal.Parse(ICMS["pICMS"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region ICMS
                                                notaitem.cdICMS_OrigMerc = "";
                                                notaitem.vlICMS_Base = 0;
                                                notaitem.cdICMS_Mod = "";
                                                notaitem.cdICMS_Trib = "";
                                                notaitem.vlICMS = 0;
                                                notaitem.vlICMS_Aliq = 0;
                                                #endregion
                                            }
                                            if (PIS != null)
                                            {
                                                #region PIS
                                                notaitem.cdPIS_CST = PIS["CST"] != null ? PIS["CST"].InnerText : "";
                                                notaitem.vlPIS = PIS["vPIS"] != null && PIS["vPIS"].InnerText != "" ? decimal.Parse(PIS["vPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Aliq = PIS["pPIS"] != null && PIS["pPIS"].InnerText != "" ? decimal.Parse(PIS["pPIS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlPIS_Base = PIS["vBC"] != null && PIS["vBC"].InnerText != "" ? decimal.Parse(PIS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {
                                                #region PIS
                                                notaitem.cdPIS_CST = "";
                                                notaitem.vlPIS = 0;
                                                notaitem.vlPIS_Aliq = 0;
                                                notaitem.vlPIS_Base = 0;
                                                #endregion
                                            }
                                            if (COFINS != null)
                                            {
                                                #region COFINS
                                                notaitem.cdCOFINS_CST = COFINS["CST"] != null ? COFINS["CST"].InnerText : "";
                                                notaitem.vlCOFINS = COFINS["vCOFINS"] != null && COFINS["vCOFINS"].InnerText != "" ? decimal.Parse(COFINS["vCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Aliq = COFINS["pCOFINS"] != null && COFINS["pCOFINS"].InnerText != "" ? decimal.Parse(COFINS["pCOFINS"].InnerText.Replace('.', ',')) : 0;
                                                notaitem.vlCOFINS_Base = COFINS["vBC"] != null && COFINS["vBC"].InnerText != "" ? decimal.Parse(COFINS["vBC"].InnerText.Replace('.', ',')) : 0;
                                                #endregion
                                            }
                                            else
                                            {

                                                #region COFINS
                                                notaitem.cdCOFINS_CST = "";
                                                notaitem.vlCOFINS = 0;
                                                notaitem.vlCOFINS_Aliq = 0;
                                                notaitem.vlCOFINS_Base = 0;
                                                #endregion
                                            }
                                            #endregion
                                        }

                                        notafiscal.listItens.Add(notaitem);
                                    }
                                    #endregion

                                    Lnotas.Add(notafiscal);

                                    escreveArquivoTextoLista("notas", Lnotas, Lnotas.Count - 1, true);
                                    //AtualizaProgresso(contador++, total, " Obtendo informação da nota " + notafiscal.nrChaveAcesso);
                                }
                                catch (Exception ex)
                                {
                                    ErroLerArquivo++;
                                    throw new Exception(ex.Message + " nomeArquivo = " + nome);
                                }

                                // Arquivo texto
                                dicionarioArquivoTexto["lastNotaProcessamento"] = (xx + 1).ToString();
                                escreveArquivoTexto("flags", dicionarioArquivoTexto);

                            }
                            else if (infEvento != null)
                            { //Verifica se é uma nota de cancelamento
                                string chNFe = infEvento["chNFe"] != null ? infEvento["chNFe"].InnerText : "";
                                if (!chNFe.Equals(""))
                                    NotasCancelamento.Add(chNFe);
                                else
                                {
                                    throw new Exception("Arquivo " + nomedoarquivo + " não está no formato conhecido!");
                                }
                            }
                            else
                            {
                                throw new Exception("Arquivo " + nomedoarquivo + " não está no formato conhecido!");
                            }
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " nomeArquivo = " + nome);

                }

                form.atual = contador;
                form.total = ls.Count - 1;
                form.CarregarProgressBar();
                contador++;

                // Arquivo texto
                startNotaProcessamento = 0;
                dicionarioArquivoTexto["lastNotaProcessamento"] = "0";
                dicionarioArquivoTexto["lastLoteProcessamento"] = (l + 1).ToString();
                escreveArquivoTexto("flags", dicionarioArquivoTexto);
            }


            total = AqCorrompido.Count();
            contador = 0;
            Thread.Sleep(1);

            #region Remove Notas Repetidas
            List<NotaFiscal> NotasAux = new List<NotaFiscal>();
            foreach (NotaFiscal nt in Lnotas)
            {
                if (!NotasAux.Any(tt => tt.nrChaveAcesso == nt.nrChaveAcesso))
                    NotasAux.Add(nt);
            }
            Lnotas = NotasAux;
            #endregion

            return Lnotas;
        }
        #endregion
        #region getFormaPgto
        private string getFormaPgto(int n)
        {
            //01 = Dinheiro 
            //02 = Cheque 
            //03 = Cartão de Crédito 
            //04 = Cartão de Débito 
            //05 = Crédito Loja 
            //10 = Vale Alimentação 
            //11 = Vale Refeição 
            //12 = Vale Presente 
            //13 = Vale Combustível 
            //99 = Outros 
            if (n == 1)
                return "Dinheiro";
            else if (n == 2)
                return "Cheque";
            else if (n == 3)
                return "Cartão de Crédito";
            else if (n == 4)
                return "Cartão de Débito";
            else if (n == 5)
                return "Crédito Loja";
            else if (n == 10)
                return "Vale Alimentação";
            else if (n == 11)
                return "Vale Refeição";
            else if (n == 12)
                return "Vale Presente";
            else if (n == 13)
                return "Vale Combustível";
            else if (n == 99)
                return "Outros";


            return "";
        }
        #endregion

        #region getBandeira
        private string getBandeira(int n)
        {

            //Informações do manual http://www.nfce.se.gov.br/portal/barra-menu/documentos/file/Manual_de_Orientacao_Contribuinte_v_6.00.zip

            if (n == 1)
                return "Visa";
            else if (n == 2)
                return "Mastercard";
            else if (n == 3)
                return "American Expres";
            else if (n == 4)
                return "Sorocred";
            else if (n == 99)
                return "Outros";
            return "";
        }
        #endregion

        #region Método que abre a conexão com o banco de dados e verifica se a venda já existe na tabela
        private void OpenConnection(Dictionary<string, string> dicionarioArquivoTexto, List<NotaFiscal> listadados, BancoDados conn, frmIReaderSEFAZ form)
        {

            int contador = 0;
            try
            {

                int total = listadados.Count;
                Thread.Sleep(1);
                listadados = listadados.OrderBy(tt => tt.nrChaveAcesso).ToList();
                //foreach (NotaFiscal nf in listadados)
                for (int i = Convert.ToInt32(dicionarioArquivoTexto["lastNotaInserida"]); i < listadados.Count; i++)
                {
                    NotaFiscal nf = listadados[i];
                    //decimal v = nf.vlTotalProdutos;
                    string script = @"select * from tax.tbNotaFiscal NF (nolock)
                                    OUTER apply (select SUM(P.vlPag) AS pgt FROM tax.tbNotaFiscalFormaPgto P (nolock) 
				                                    WHERE P.idNota = NF.idNota) PG
                                    where NF.nrChaveAcesso = @chave
                                    AND (vlTotalNFe - PG.pgt) = 0";

                    //SqlCommand cmd = new SqlCommand(script, conn);
                    //cmd.CommandTimeout = 60;
                    List<SqlParameter> parametrosTT = new List<SqlParameter>();
                    parametrosTT.Add(new SqlParameter("@chave", nf.nrChaveAcesso));

                    DataTable dtDados = new DataTable();
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //da.Fill(dtDados);
                    dtDados = conn.SqlQueryDataTable(nf.nrCNPJEmt, script, parametrosTT, 60);

                    if (dtDados.Rows.Count == 0)
                    {
                        try
                        {
                            DeleteNTErro(nf.nrChaveAcesso, nf.nrCNPJEmt, conn);
                            Inserir(nf, conn);
                            inseridos++;
                            //AtualizaProgresso(nf, "Registro adicionado.", contador++, total);

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    else
                    {
                        //UpdateXMLName(nf.nrChaveAcesso, nf.XMLname, nf.dtEmissao, conn, nf.nrCNPJEmt);
                        //AtualizaProgresso(nf, "Não adicionado, dado duplicado.", contador++, total);
                        naoinseridos++;
                    }
                    form.atual = contador;
                    form.total = listadados.Count - 1;
                    form.CarregarProgressBar();
                    contador++;
                    dicionarioArquivoTexto["lastNotaInserida"] = (i + 1).ToString();
                    escreveArquivoTexto("flags", dicionarioArquivoTexto);
                }
                //AtualizaProgresso(null, "", contador++, total);
                if (NotasCancelamento != null && NotasCancelamento.Count > 0)
                    CancelarNota(NotasCancelamento, conn);
            }
            catch (Exception e)
            {
                //MessageBox.Show(" Erro: " + e.Message);
                //AtualizaProgresso(0, 0, " Erro: " + e.Message);
                throw new Exception(e.Message);
            }
            //Altera o status das notas canceladas


        }

        #endregion

        #region Cancelamento da nota
        private void CancelarNota(List<string> lnotas, BancoDados conn)
        {
            Thread.Sleep(1);
            foreach (string chave in lnotas)
            {
                UpdateNotaCancelamento(chave, conn);
            }
        }
        #endregion

        #region UpdateNotaCancelamento
        private void UpdateNotaCancelamento(string chave, BancoDados conn)
        {

            string cnpj = BancoDadosUtils.ConvertDataTableToDataType<string>(conn.SqlQueryDataTable(@"select nrCNPJEmt from  tax.tbNotafiscal N (nolock)
                                                                    where N.nrCHaveAcesso ='" + chave + "'")).FirstOrDefault();
            string script = @"update Tax.tbNotaFiscal
								SET Status = 'C'
								where nrChaveAcesso = @nrChaveAcesso";
            //SqlCommand cmd = new SqlCommand(script, conn);
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@nrChaveAcesso", chave));

            conn.ExecuteScript("tax.tbnotafiscal", cnpj, script, parametrosTT);
        }
        #endregion

        #region DeleteNTErro
        private void DeleteNTErro(BancoDados conn, string nrCNPJ)
        {

            string script = @"select idNota into #TABLE from tax.tbNotaFiscal (nolock)
								where idNota not in(
								select N.idNota from tax.tbNotaFiscal (nolock) N
								inner join tax.tbNotaFiscalFormaPgto (nolock) P on(p.idNota =n.idNota)
								);

								delete tax.tbNotaFiscalItem
								where idNota in(
								select idNota from #TABLE
								);

								delete tax.tbNotaFiscal
								where idNota in(
								select idNota from #TABLE
								);

								Drop table #TABLE;";
            //SqlCommand cmd = new SqlCommand(script, conn);
            conn.ExecuteScript("tax.tbNotaFiscal", nrCNPJ, script);
        }
        #endregion

        private void DeleteNTErro(string nrChaveAcesso, string nrCNPJ, BancoDados conn)
        {
            string script = "if exists (select idNota from tax.tbNotaFiscal (nolock) where nrChaveAcesso = '" + nrChaveAcesso + "')" +
                                " delete tax.tbNotaFiscal where nrChaveAcesso = '" + nrChaveAcesso + "'";


            //SqlCommand cmd = new SqlCommand(script, conn);
            conn.ExecuteScript("tax.tbNotaFiscal", nrCNPJ, script);
        }

        #region InsertPgto
        private void InsertPgto(NotaFiscalFormaPgto nfpg, BancoDados conn, string cnpj)
        {

            string script = @"INSERT INTO tax.tbNotaFiscalFormaPgto(cdFormaPgto,dsFormaPgto,dsBandeira,nrAutorizacao,idNota,vlPag)
							VALUES (@cdFormaPgto,@dsFormaPgto,@dsBandeira,@nrAutorizacao,@idNota,@vlPag)";
            //SqlCommand cmd = new SqlCommand(script, conn);
            //cmd.CommandTimeout = 60;
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@cdFormaPgto", nfpg.cdFormaPgto));
            parametrosTT.Add(new SqlParameter("@dsFormaPgto", nfpg.dsFormaPgto));
            if (nfpg.dsBandeira != null)
                parametrosTT.Add(new SqlParameter("@dsBandeira", nfpg.dsBandeira));
            else
                parametrosTT.Add(new SqlParameter("@dsBandeira", DBNull.Value));
            parametrosTT.Add(new SqlParameter("@nrAutorizacao", nfpg.nrAutorizacao));
            parametrosTT.Add(new SqlParameter("@idNota", nfpg.idNota));
            parametrosTT.Add(new SqlParameter("@vlPag", nfpg.vlPag));

            conn.ExecuteScript("tax.tbnotafiscalformapgto", cnpj, script, parametrosTT, 60);
        }
        #endregion

        #region Método para inserir os dados na tabela tbNotaFiscal
        private void Inserir(NotaFiscal nf, BancoDados conn)
        {


            try
            {
                conn.BeginTransaction();
                string script = @"INSERT INTO tax.tbNotaFiscal
									   (XML,nrCNPJEmt,dtEmissao,nrChaveAcesso,nrNota,nrModelo,nrSerie,dsRazaoSocialEmt,dsProcesso,dsNaturezaOpr
									   ,dsRazaoSocialDest,nrCnpjCpfDest,nrProtocolo,vlBaseICMS,vlICMS,vlICMSDesonerado,vlBaseICMSST
								   ,vlICMSSub,vlTotalProdutos,vlFrete,vlSeguro,vlDespAcess,vlTotalIPI,vlTotalNFe,vlTotalDesconto
									   ,vlTotalII,vlPIS,vlCOFINS,vlAproxTributo,cdNota,vlOutrasDesp,dtContigencia,XMLName)
								 VALUES
									   (@xml,@nrCNPJEmt,@dtEmissao,@nrChaveAcesso,@nrNota,@nrModelo,@nrSerie,@dsRazaoSocialEmt,@dsProcesso
									   ,@dsNaturezaOpr,@dsRazaoSocialDest,@nrCnpjCpfDest,@nrProtocolo,@vlBaseICMS,@vlICMS,@vlICMSDesonerado
									   ,@vlBaseICMSST,@vlICMSSub,@vlTotalProdutos,@vlFrete,@vlSeguro,@vlDespAcess,@vlTotalIPI,@vlTotalNFe 
								 	   ,@vlTotalDesconto,@vlTotalII,@vlPIS,@vlCOFINS,@vlAproxTributo,@cdNota,@vlOutrasDesp,@dtContigencia,@XMLName);
								 SELECT SCOPE_IDENTITY() as ID;";
                //SqlCommand cmd = new SqlCommand(script, conn);
                //cmd.CommandTimeout = 60;
                List<SqlParameter> parametrosTT = new List<SqlParameter>();

                if (nf.xml != null)
                    parametrosTT.Add(new SqlParameter("@xml", nf.xml));
                else
                    parametrosTT.Add(new SqlParameter("@xml", DBNull.Value));

                parametrosTT.Add(new SqlParameter("@nrCNPJEmt", nf.nrCNPJEmt));
                parametrosTT.Add(new SqlParameter("@dtEmissao", nf.dtEmissao));
                parametrosTT.Add(new SqlParameter("@nrChaveAcesso", nf.nrChaveAcesso));
                parametrosTT.Add(new SqlParameter("@nrNota", nf.nrNota));
                parametrosTT.Add(new SqlParameter("@nrModelo", nf.nrModelo));
                parametrosTT.Add(new SqlParameter("@nrSerie", nf.nrSerie));
                parametrosTT.Add(new SqlParameter("@dsRazaoSocialEmt", nf.dsRazaoSocialEmt));

                if (nf.dsProcesso != null)
                    parametrosTT.Add(new SqlParameter("@dsProcesso", nf.dsProcesso));
                else
                    parametrosTT.Add(new SqlParameter("@dsProcesso", DBNull.Value));
                if (nf.dsNaturezaOpr != null)
                    parametrosTT.Add(new SqlParameter("@dsNaturezaOpr", nf.dsNaturezaOpr));
                else
                    parametrosTT.Add(new SqlParameter("@dsNaturezaOpr", DBNull.Value));

                if (nf.dsRazaoSocialDest != null)
                    parametrosTT.Add(new SqlParameter("@dsRazaoSocialDest", nf.dsRazaoSocialDest));
                else
                    parametrosTT.Add(new SqlParameter("@dsRazaoSocialDest", DBNull.Value));
                if (nf.nrCnpjCpfDest != null)
                    parametrosTT.Add(new SqlParameter("@nrCnpjCpfDest", nf.nrCnpjCpfDest));
                else
                    parametrosTT.Add(new SqlParameter("@nrCnpjCpfDest", DBNull.Value));

                if (nf.nrProtocolo != null)
                    parametrosTT.Add(new SqlParameter("@nrProtocolo", nf.nrProtocolo));
                else
                    parametrosTT.Add(new SqlParameter("@nrProtocolo", DBNull.Value));

                parametrosTT.Add(new SqlParameter("@vlBaseICMS", nf.vlBaseICMS));
                parametrosTT.Add(new SqlParameter("@vlICMS", nf.vlICMS));
                parametrosTT.Add(new SqlParameter("@vlICMSDesonerado", nf.vlICMSDesonerado));
                parametrosTT.Add(new SqlParameter("@vlBaseICMSST", nf.vlBaseICMSST));
                parametrosTT.Add(new SqlParameter("@vlICMSSub", nf.vlICMSSub));
                parametrosTT.Add(new SqlParameter("@vlTotalProdutos", nf.vlTotalProdutos));
                parametrosTT.Add(new SqlParameter("@vlFrete", nf.vlFrete));
                parametrosTT.Add(new SqlParameter("@vlSeguro", nf.vlSeguro));
                parametrosTT.Add(new SqlParameter("@vlDespAcess", nf.vlDespAcess));
                parametrosTT.Add(new SqlParameter("@vlTotalIPI", nf.vlTotalIPI));
                parametrosTT.Add(new SqlParameter("@vlTotalNFe", nf.vlTotalNFe));
                parametrosTT.Add(new SqlParameter("@vlTotalDesconto", nf.vlTotalDesconto));
                parametrosTT.Add(new SqlParameter("@vlTotalII", nf.vlTotalII));
                parametrosTT.Add(new SqlParameter("@vlPIS", nf.vlPIS));
                parametrosTT.Add(new SqlParameter("@vlCOFINS", nf.vlCOFINS));
                parametrosTT.Add(new SqlParameter("@vlAproxTributo", nf.vlAproxTributo));
                parametrosTT.Add(new SqlParameter("@cdNota", nf.cdNota));
                parametrosTT.Add(new SqlParameter("@vlOutrasDesp", nf.vlOutrasDesp));
                if (nf.dtContigencia != null)
                    parametrosTT.Add(new SqlParameter("@dtContigencia", nf.dtContigencia));
                else
                    parametrosTT.Add(new SqlParameter("@dtContigencia", DBNull.Value));

                if (nf.XMLname != null)
                    parametrosTT.Add(new SqlParameter("@XMLName", nf.XMLname));
                else
                    parametrosTT.Add(new SqlParameter("@XMLName", DBNull.Value));


                int idNota = Convert.ToInt32(conn.ExecuteScriptScalar("tax.tbNotaFiscal", nf.nrCNPJEmt, script, parametrosTT, 60));


                foreach (NotaFiscalFormaPgto nfpg in nf.listFormaPgto)
                {
                    nfpg.idNota = idNota;
                    InsertPgto(nfpg, conn, nf.nrCNPJEmt);
                }

                List<Tbitem> itens = new List<Tbitem>();

                foreach (NotaFiscalItem nfitem in nf.listItens)
                {
                    nfitem.idNota = idNota;

                    InsertItem(nfitem, conn, nf.nrCNPJEmt);

                    Tbitem item = new Tbitem();
                    item.nrCNPJ = nf.nrCNPJEmt;
                    item.dtInsert = DateTime.Now;
                    item.cdProduto = nfitem.cdProduto.ToString();
                    item.dsItem = nfitem.dsItem;
                    item.dtInicio = DateTime.Parse("01-01-" + (DateTime.Now.Year - 1).ToString());
                    item.idUserInsert = 290;
                    itens.Add(item);
                }

                foreach (Tbitem i in itens)
                    if (IsExiste(i) == false)
                        Insert_Tbitem(i);

                conn.CommitTransaction();
            }
            catch (Exception ex)
            {
                conn.RollbackTransaction();
                throw new Exception("Falha ao inserir a nota " + nf.nrChaveAcesso + " na base da Atos capital!");
            }
        }
        #endregion

        #region IsExiste
        private Boolean? IsExiste(Tbitem objeto)
        {
            Boolean? ver = false;
            using (BancoDados conn = new BancoDados(true))
            {

                try
                {
                    string script_select = @"SELECT [dbo].[FN_cdProdutoExiste]('" + objeto.nrCNPJ + "', '" + objeto.cdProduto + "','" + ((DateTime)objeto.dtInicio).ToString("yyyyMMdd") + "')";


                    DataTable dtDados = new DataTable();
                    dtDados = conn.SqlQueryDataTable(objeto.nrCNPJ, script_select, null, 60);

                    List<Tbitem> tbitem = new List<Tbitem>();
                    Thread.Sleep(1);
                    for (int i = 0; i < dtDados.Rows.Count; i++)
                    {
                        ver = dtDados.Rows[i][0] != DBNull.Value ? (Boolean?)dtDados.Rows[i][0] : false;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Falha ao consultar se o item existem");
                }

            }
            return ver;
        }

        #endregion

        #region UpdateXMLName
        private void UpdateXMLName(string chave, string name, DateTime dtEmissao, BancoDados conn, string cnpj)
        {

            string script = @"update Tax.tbNotaFiscal
								SET --XMLName = '" + name + @"', 
                                dtEmissao  = @dtEmissao
								where nrChaveAcesso = @nrChaveAcesso";
            //SqlCommand cmd = new SqlCommand(script, conn);
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@nrChaveAcesso", chave));
            parametrosTT.Add(new SqlParameter("@dtEmissao", dtEmissao));
            conn.ExecuteScript("tax.tbnotafiscal", cnpj, script, parametrosTT);
        }
        #endregion

        #region InsertItem
        private void InsertItem(NotaFiscalItem nfItem, BancoDados conn, string cnpj)
        {

            string script = @"INSERT INTO tax.tbNotaFiscalItem
								   (idNota,dsItem,vlTotalItem,cdProduto,cdNCM,cdCFOP,dsUnidadeComercial,qtItem
								   ,vlUntComercial,cdEANComercial,cdEANTributavel,vlUntTributacao,vlAproxTributo
								   ,nrItem,cdICMS_OrigMerc,cdICMS_Trib,cdICMS_Mod,vlICMS_Base,vlICMS_Aliq,vlICMS
								   ,cdPIS_CST,vlPIS_Base,vlPIS_Aliq,vlPIS,cdCOFINS_CST,vlCOFINS_Base,vlCOFINS_Aliq
								   ,vlCOFINS)
							 VALUES
								   (@idNota,@dsItem,@vlTotalItem,@cdProduto,@cdNCM,@cdCFOP,@dsUnidadeComercial,@qtItem
								   ,@vlUntComercial,@cdEANComercial,@cdEANTributavel,@vlUntTributacao,@vlAproxTributo
								   ,@nrItem,@cdICMS_OrigMerc,@cdICMS_Trib,@cdICMS_Mod,@vlICMS_Base,@vlICMS_Aliq,@vlICMS
								   ,@cdPIS_CST,@vlPIS_Base,@vlPIS_Aliq,@vlPIS,@cdCOFINS_CST,@vlCOFINS_Base,@vlCOFINS_Aliq
								   ,@vlCOFINS)";
            //SqlCommand cmd = new SqlCommand(script, conn);
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@idNota", nfItem.idNota));
            parametrosTT.Add(new SqlParameter("@dsItem", nfItem.dsItem));
            parametrosTT.Add(new SqlParameter("@vlTotalItem", nfItem.vlTotalItem));
            parametrosTT.Add(new SqlParameter("@cdProduto", nfItem.cdProduto));
            parametrosTT.Add(new SqlParameter("@cdNCM", nfItem.cdNCM));
            parametrosTT.Add(new SqlParameter("@cdCFOP", nfItem.cdCFOP));
            parametrosTT.Add(new SqlParameter("@dsUnidadeComercial", nfItem.dsUnidadeComercial));
            parametrosTT.Add(new SqlParameter("@qtItem", nfItem.qtItem));
            parametrosTT.Add(new SqlParameter("@vlUntComercial", nfItem.vlUntComercial));
            parametrosTT.Add(new SqlParameter("@cdEANComercial", nfItem.cdEANComercial));
            parametrosTT.Add(new SqlParameter("@cdEANTributavel", nfItem.cdEANTributavel));
            parametrosTT.Add(new SqlParameter("@vlUntTributacao", nfItem.vlUntTributacao));
            parametrosTT.Add(new SqlParameter("@vlAproxTributo", nfItem.vlAproxTributo));
            parametrosTT.Add(new SqlParameter("@nrItem", nfItem.nrItem));
            if (nfItem.cdICMS_OrigMerc != null)
                parametrosTT.Add(new SqlParameter("@cdICMS_OrigMerc", nfItem.cdICMS_OrigMerc));
            else
                parametrosTT.Add(new SqlParameter("@cdICMS_OrigMerc", DBNull.Value));
            if (nfItem.cdICMS_Trib != null)
                parametrosTT.Add(new SqlParameter("@cdICMS_Trib", nfItem.cdICMS_Trib));
            else
                parametrosTT.Add(new SqlParameter("@cdICMS_Trib", DBNull.Value));
            if (nfItem.cdICMS_Mod != null)
                parametrosTT.Add(new SqlParameter("@cdICMS_Mod", nfItem.cdICMS_Mod));
            else
                parametrosTT.Add(new SqlParameter("@cdICMS_Mod", DBNull.Value));
            parametrosTT.Add(new SqlParameter("@vlICMS_Base", nfItem.vlICMS_Base));
            parametrosTT.Add(new SqlParameter("@vlICMS_Aliq", nfItem.vlICMS_Aliq));
            parametrosTT.Add(new SqlParameter("@vlICMS", nfItem.vlICMS));
            parametrosTT.Add(new SqlParameter("@cdPIS_CST", nfItem.cdPIS_CST));
            parametrosTT.Add(new SqlParameter("@vlPIS_Base", nfItem.vlPIS_Base));
            parametrosTT.Add(new SqlParameter("@vlPIS_Aliq", nfItem.vlPIS_Aliq));
            parametrosTT.Add(new SqlParameter("@vlPIS", nfItem.vlPIS));
            parametrosTT.Add(new SqlParameter("@cdCOFINS_CST", nfItem.cdCOFINS_CST));
            parametrosTT.Add(new SqlParameter("@vlCOFINS_Base", nfItem.vlCOFINS_Base));
            parametrosTT.Add(new SqlParameter("@vlCOFINS_Aliq", nfItem.vlCOFINS_Aliq));
            parametrosTT.Add(new SqlParameter("@vlCOFINS", nfItem.vlCOFINS));

            //cmd.CommandTimeout = 60;
            conn.ExecuteScript("tax.tbnotafiscalitem", cnpj, script, parametrosTT, 60);


        }
        #endregion

        #region Insert_Tbitem
        //INSERT 
        private void Insert_Tbitem(Tbitem objeto)
        {
            try
            {
                using (BancoDados conn = new BancoDados(true))
                {
                    string script_insert = @"INSERT INTO tax.tbItem(cdGrupo,dtInicio,dtInsert,cdProduto,dsItem,nrCNPJ,idUserInsert) " +
                        "VALUES((select top 1 id_grupo from cliente.empresa where nu_cnpj = '" + objeto.nrCNPJ + "'), " +
                         (objeto.dtInicio != null ? "   '" + ((DateTime)objeto.dtInicio).ToString("yyyyMMdd") + "'," : "null,") +
                         (objeto.dtInsert != null ? "   '" + ((DateTime)objeto.dtInsert).ToString("yyyyMMdd") + "'," : "null,") +
                         "   '" + objeto.cdProduto + "'," +
                         "   '" + objeto.dsItem + "'," +
                         "   '" + objeto.nrCNPJ + "'," +
                         "   " + objeto.idUserInsert + ")";

                    conn.ExecuteScript("tax.tbitem", objeto.nrCNPJ, script_insert, null, 60);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Falha ao inserir o item");
            }
        }
        #endregion

        #region Converter
        private string Converter(string linha)
        {
            if ((!linha.All(char.IsNumber) && linha.Length <= 3) && (!linha.ToLower().Contains("x") && (!linha.Contains(","))))
                linha = "";
            return linha;
        }
        #endregion      

        #region Método para inserir os dados na tabela tbArquivo
        private void Inserir(Arquivo arq, BancoDados conn)
        {
            string script = @"INSERT INTO tax.tbArquivo(cdFilial,dtEmissao,hora,cdArquivo,cdVenda,ArquivoName,vlVenda,vlDesconto,vlPag)
								VALUES(@cdFilial,@dtEmissao,@hora,@cdArquivo,@cdVenda,@ArquivoName,@vlVenda,@vlDesconto,@vlPag);
								 SELECT SCOPE_IDENTITY() as ID;";
            //SqlCommand cmd = new SqlCommand(script, conn);
            //cmd.CommandTimeout = 60;
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@cdFilial", arq.cdFilial));
            parametrosTT.Add(new SqlParameter("@dtEmissao", arq.dtVenda));
            parametrosTT.Add(new SqlParameter("@hora", arq.hora));
            parametrosTT.Add(new SqlParameter("@cdArquivo", arq.cdArquivo));
            parametrosTT.Add(new SqlParameter("@cdVenda", arq.cdVenda));
            parametrosTT.Add(new SqlParameter("@ArquivoName", arq.nomeArquivo));
            parametrosTT.Add(new SqlParameter("@vlVenda", arq.vlVenda));
            parametrosTT.Add(new SqlParameter("@vlDesconto", arq.vlDesconto));
            parametrosTT.Add(new SqlParameter("@vlPag", arq.vlPag));


            int idArquivo = Convert.ToInt32(conn.ExecuteScriptScalar("tax.tbArquivo", arq.cdGrupo, script, parametrosTT, 60));

            Thread.Sleep(1);
            foreach (ArquivoPag Arqpg in arq.Pagamentos)
            {
                Arqpg.idArquivo = idArquivo;
                InsertPgto(Arqpg, arq.cdGrupo, conn);
            }


        }
        #endregion

        #region InsertPgto
        private void InsertPgto(ArquivoPag ArqPag, int cdGrupo, BancoDados conn)
        {

            string script = @"INSERT INTO tax.tbArquivoPgto(cdSacado,dsBandeira,idArquivo,vlPag,qtParcelas)
								VALUES(@cdSacado,@dsBandeira,@idArquivo,@vlPag,@qtParcelas)";
            //SqlCommand cmd = new SqlCommand(script, conn);
            //cmd.CommandTimeout = 60;
            List<SqlParameter> parametrosTT = new List<SqlParameter>();
            parametrosTT.Add(new SqlParameter("@cdSacado", ArqPag.cdSacado));
            parametrosTT.Add(new SqlParameter("@dsBandeira", ArqPag.dsBandeira));
            parametrosTT.Add(new SqlParameter("@idArquivo", ArqPag.idArquivo));
            parametrosTT.Add(new SqlParameter("@vlPag", ArqPag.vlPag));
            parametrosTT.Add(new SqlParameter("@qtParcelas", ArqPag.qtParcela));

            conn.ExecuteScript("tax.tbarquivopgto", cdGrupo, script, parametrosTT, 60);
        }
        #endregion      

    }

    public class NotaFiscal
    {
        public int idNota { get; set; }//OK
        public string nrCNPJEmt { set; get; }//OK
        public DateTime dtEmissao { set; get; }//OK
        public Nullable<DateTime> dtContigencia { set; get; }//OK
        public string nrChaveAcesso { set; get; }//OK
        public string cdNota { set; get; }//OK
        public int nrNota { set; get; }//OK
        public int nrModelo { set; get; }//OK
        public int nrSerie { set; get; }//OK
        public string dsRazaoSocialEmt { set; get; }//OK
        public string dsProcesso { set; get; }//OK
        public string dsNaturezaOpr { set; get; }//OK
        public string dsRazaoSocialDest { set; get; }
        public string nrCnpjCpfDest { set; get; }//OK
        public string nrProtocolo { set; get; }//OK
        public decimal vlBaseICMS { set; get; }//OK
        public decimal vlICMS { set; get; }//OK
        public decimal vlICMSDesonerado { set; get; }//Ok
        public decimal vlBaseICMSST { set; get; }//OK
        public decimal vlICMSSub { set; get; }//OK
        public decimal vlTotalProdutos { set; get; }//OK
        public decimal vlFrete { set; get; }//OK
        public decimal vlSeguro { set; get; }//OK
        public decimal vlDespAcess { set; get; }//OK
        public decimal vlTotalII { set; get; }//OK
        public decimal vlTotalIPI { set; get; }//OK		
        public decimal vlPIS { set; get; }//OK
        public decimal vlCOFINS { set; get; }//OK
        public decimal vlTotalNFe { set; get; }//OK
        public decimal vlTotalDesconto { set; get; }// OK 
        public decimal vlOutrasDesp { set; get; }
        public decimal vlAproxTributo { set; get; }
        public string XMLname { set; get; }
        public string xMotivo { get; set; }
        public List<NotaFiscalFormaPgto> listFormaPgto { get; set; }
        public List<NotaFiscalItem> listItens { get; set; }
        public string xml { get; set; }

        public NotaFiscal()
        {
            listFormaPgto = new List<NotaFiscalFormaPgto>();
            listItens = new List<NotaFiscalItem>();
        }
    }
    public class NotaFiscalFormaPgto
    {
        public int idFormaPgto { set; get; }
        public int cdFormaPgto { set; get; }
        public string dsFormaPgto { set; get; }
        public string dsBandeira { set; get; }
        public int nrAutorizacao { set; get; }
        public int idNota { set; get; }
        public decimal vlPag { set; get; }

    }
    public class NotaFiscalItem
    {
        public int idNota { get; set; }
        public int idNotaItem { get; set; }
        public string dsItem { get; set; }//OK
        public decimal vlTotalItem { get; set; }//OK
        public string cdProduto { get; set; }//OK
        public int cdNCM { get; set; }//OK
        public int cdCFOP { get; set; }//OK
        public string dsUnidadeComercial { get; set; }//OK
        public decimal qtItem { get; set; }//OK
        public decimal vlUntComercial { get; set; }//OK
        public int cdEANComercial { get; set; }//OK*
        public int cdEANTributavel { get; set; }//OK
        public decimal vlUntTributacao { get; set; }//OK
        public decimal vlAproxTributo { get; set; }
        public int nrItem { get; set; }//OK
        public string cdICMS_OrigMerc { get; set; }//OK
        public string cdICMS_Trib { get; set; }//OK
        public string cdICMS_Mod { get; set; }//OK
        public decimal vlICMS_Base { get; set; }//OK
        public decimal vlICMS_Aliq { get; set; }
        public decimal vlICMS { get; set; }//OK
        public string cdPIS_CST { get; set; }//OK
        public decimal vlPIS_Base { get; set; }//OK
        public decimal vlPIS_Aliq { get; set; }//OK
        public decimal vlPIS { get; set; }//OK
        public string cdCOFINS_CST { get; set; }//OK
        public decimal vlCOFINS_Base { get; set; }//OK
        public decimal vlCOFINS_Aliq { get; set; }//OK
        public decimal vlCOFINS { get; set; }//OK

    }
    public class Tbitem
    {
        public DateTime? dtInicio { get; set; }
        public DateTime? dtInsert { get; set; }
        public string cdProduto { get; set; }
        public string dsItem { get; set; }
        public string nrCNPJ { get; set; }
        public int? idUserInsert { get; set; }
    }
    public class Arquivo
    {
        public int cdGrupo { get; set; }
        public string cdFilial { get; set; }
        public DateTime dtVenda { get; set; }
        public string hora { get; set; }
        public string cdArquivo { get; set; }
        public string cdVenda { get; set; }
        public decimal vlVenda { get; set; }
        public decimal vlDesconto { get; set; }
        public decimal vlPag { get; set; }
        public List<ArquivoPag> Pagamentos { get; set; }
        public string nomeArquivo { get; set; }
        public int idArquivo { get; set; }

        public Arquivo()
        {
            Pagamentos = new List<ArquivoPag>();
        }
    }
    public class ArquivoPag
    {
        public string cdSacado { get; set; }
        public string dsBandeira { get; set; }
        public decimal vlPag { get; set; }
        public int qtParcela { get; set; }
        public int idArquivoPag { get; set; }
        public int idArquivo { get; set; }
    }
}
