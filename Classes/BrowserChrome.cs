﻿using CefSharp;
using CefSharp.WinForms;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace IReaderSEFAZAM.Classes
{
    public class BrowserChrome
    {
        private System.Threading.Timer timer;
        private ChromiumWebBrowser webBrowser;
        private Process processoCEF;
        private bool finish;
        private string error;
        private Semaphore semaforo;
        private string pageSource;
        private Semaphore wait;
        private string[] urlsEsperadas;
        private string xpathExpected;
        private int minElemExpected;
        private EXPECTEDCONDITION conditionExpected;


        public delegate void EventLoading(bool visivel);
        public bool BrowserLoading2;

        public enum EXPECTEDCONDITION
        {
            VISIBLE = 0,
            INVISIBLE = 1,
            PRESENCE = 2,
        }

        #region Esperar componete
        public bool Esperar2(string xpath, string otherXpath, int time = 60, int time2 = 10)
        {
            bool carregou = false;

            for (int i = 0; i <= time * 10; i++)
            {
                System.Threading.Thread.Yield();
                if (i == (time * 10))
                {
                    carregou = false;
                }
                Thread.Sleep(100);

                List<HtmlAgilityPack.HtmlDocument> documents = getDocument2(time2);

                foreach (HtmlAgilityPack.HtmlDocument document in documents)
                {
                    System.Threading.Thread.Yield();
                    HtmlNodeCollection elements = document.DocumentNode.SelectNodes(xpath);
                    if (elements != null && elements.Count > 0) { carregou = true; break; }
                    if (otherXpath != null)
                    {
                        elements = document.DocumentNode.SelectNodes(otherXpath);
                        if (elements != null && elements.Count > 0) { carregou = true; break; }
                    }

                }

                if (carregou)
                    break;
            }
            return carregou;
        }
        #endregion
        #region getDocumento2
        public List<HtmlAgilityPack.HtmlDocument> getDocument2(int time)
        {
            List<HtmlAgilityPack.HtmlDocument> documents = new List<HtmlAgilityPack.HtmlDocument>();
            try
            {
                List<string> pages = this.PagesSourceSync(null, time);

                foreach (string pag in pages)
                {
                    System.Threading.Thread.Yield();
                    HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                    document.LoadHtml(pag);
                    documents.Add(document);
                }

                return documents;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível carregar o document!");
            }

        }
        #endregion
        #region Esperar componete
        public void Esperar(int time = 60, int time2 = 10)
        {

            for (int i = 0; i <= time * 10; i++)
            {
                Thread.Sleep(time2);

                List<HtmlAgilityPack.HtmlDocument> documents = getDocument2(time);

                foreach (HtmlAgilityPack.HtmlDocument document in documents)
                {
                    Thread.Sleep(1);
                }


            }

        }
        #endregion
        #region IsTextoExiste
        public bool IsTextoExiste(string txt, string framename, int time)
        {
            bool existe = false;
            int inicial = 0;
            Thread.Sleep(1);
            while (true)
            {
                HtmlDocument document = new HtmlDocument();
                List<string> paginas = this.PagesSourceSync(framename, 10);
                foreach (string pag in paginas)
                {
                    document.LoadHtml(pag);
                    if (document.DocumentNode.SelectNodes("//*[contains(text(), '" + txt + "')]") != null)
                    {
                        existe = true;
                        break;
                    }


                }
                if (existe)
                    break;
                if (inicial >= time)
                {
                    break;
                }
                inicial++;
                Thread.Sleep(1000);
            }
            return existe;
        }
        #endregion
        public BrowserChrome(ChromiumWebBrowser webBrowser)
        {
            this.webBrowser = webBrowser;
            this.processoCEF = processoCEF;
            this.urlsEsperadas = null;
            this.xpathExpected = null;
            //this.downloadExtension = null;
            this.pageSource = null;
            this.error = null;
            this.finish = false;
            this.minElemExpected = 0;
            //this.file = null;

            this.semaforo = new Semaphore(1, 1);
            this.wait = new Semaphore(0, 1);
            this.webBrowser.LifeSpanHandler = new LifeSpanHandler(null, false, false);
            BrowserLoading2 = false;


        }

        public bool CarregaImagem()
        {
            return webBrowser == null ? false : webBrowser.BrowserSettings.ImageLoading.Equals(CefState.Enabled);
        }

        public void SetJavaScriptEnable(bool enable)
        {
            webBrowser.BrowserSettings.Javascript = enable ? CefState.Enabled : CefState.Disabled;
        }


        public bool ProcessoCEFExiste()
        {
            return true;
            //if (processoCEF == null) return false;
            //bool processoExiste = false;
            //try
            //{
            //    processoExiste = Process.GetProcessById(processoCEF.Id) != null;
            //}
            //catch { }
            //return processoExiste;
        }


        public string PageSourceSync(long frameIdentifier, int timeoutSeconds = 10)
        {
            pageSource = null;
            if (webBrowser == null) return String.Empty;

            int cont = 0;
            if (!webBrowser.IsBrowserInitialized && cont < 10) // 10 segundos pra inicializar
            {
                Thread.Sleep(1000);
                cont++;
            }
            if (!webBrowser.IsBrowserInitialized)
            {
                throw new Exception("Browser não inicializado corretamente!");
            }

            Task<string> task;
            if (frameIdentifier < 0) task = webBrowser.GetBrowser().MainFrame.GetSourceAsync();
            else task = webBrowser.GetBrowser().GetFrame(frameIdentifier).GetSourceAsync();

            if (!task.IsCompleted)
            {
                if (!task.Status.Equals(TaskStatus.Running))
                {
                    try
                    {
                        task.Start();
                    }
                    catch { }
                }
                if (!task.Wait(TimeSpan.FromSeconds(timeoutSeconds)))
                {
                    throw new Exception("Tempo limite atingido!");
                }
            }
            pageSource = task.Result;
            BrowserLoading2 = false;
            return pageSource;
        }

        public string PageSourceSync(string frameName = null, int timeoutSeconds = 10)
        {
            pageSource = null;
            if (webBrowser == null) return String.Empty;

            int cont = 0;
            if (!webBrowser.IsBrowserInitialized && cont < 10) // 10 segundos pra inicializar
            {
                Thread.Sleep(1000);
                cont++;
            }
            if (!webBrowser.IsBrowserInitialized)
            {
                throw new Exception("Browser não inicializado corretamente!");
            }

            Task<string> task;
            if (frameName == null) task = webBrowser.GetBrowser().MainFrame.GetSourceAsync();
            else task = webBrowser.GetBrowser().GetFrame(frameName).GetSourceAsync();

            if (!task.IsCompleted)
            {
                if (!task.Status.Equals(TaskStatus.Running))
                {
                    try
                    {
                        task.Start();

                    }
                    catch (Exception ex) { }
                }
                //if (!task.Wait(TimeSpan.FromSeconds(timeoutSeconds)))
                //{
                //	throw new Exception("Tempo limite atingido!");
                //}
            }
            pageSource = task.Result;
            BrowserLoading2 = false;

            return pageSource;
        }

        public List<string> PagesSourceSync(string frameName = null, int timeoutSeconds = 10)
        {
            pageSource = null;
            if (webBrowser == null) return null;

            int cont = 0;
            if (!webBrowser.IsBrowserInitialized && cont < 10) // 10 segundos pra inicializar
            {
                Thread.Sleep(1000);
                cont++;
            }
            if (!webBrowser.IsBrowserInitialized)
            {
                throw new Exception("Browser não inicializado corretamente!");
            }

            Task<string> task;

            if (frameName == null) task = webBrowser.GetBrowser().MainFrame.GetSourceAsync();
            else task = webBrowser.GetBrowser().GetFrame(frameName).GetSourceAsync();

            if (!task.IsCompleted)
            {
                if (!task.Status.Equals(TaskStatus.Running))
                {
                    try
                    {
                        task.Start();

                    }
                    catch (Exception ex) { }
                }

            }

            BrowserLoading2 = false;
            List<string> pagesSource = new List<string>();
            try
            {
                var id = webBrowser.GetBrowser().GetFrameIdentifiers();
                foreach (var vv in id)
                {
                    System.Threading.Thread.Yield();
                    string pag = webBrowser.GetBrowser().GetFrame(vv).GetSourceAsync().Result;
                    pagesSource.Add(pag);
                }
            }
            catch (Exception e)
            {
            }
            return pagesSource;
        }

        /* ASYNCHRONOUS
        private async Task<string> PageSource(IFrame frame, int timeoutSeconds)
        {
            pageSource = null;
            if (frame == null) return null;//"";
            int cont = 0;
            while (cont < timeoutSeconds * 100 && webBrowser.IsLoading) System.Threading.Thread.Yield();
            if (webBrowser.IsLoading)
                throw new Exception("Tempo limite atingido");
            pageSource = await frame.GetSourceAsync();
            //pageSource = await GetSourceAsync(frame.Name);
            return pageSource;
        }
        
        public async Task<string> PageSource(long frameIdentifier, int timeoutSeconds = 10)
        {
            if (webBrowser == null) return String.Empty;
            if (frameIdentifier < 0) return await PageSource(webBrowser.GetBrowser().MainFrame, timeoutSeconds);
            return await PageSource(webBrowser.GetBrowser().GetFrame(frameIdentifier), timeoutSeconds);
        }

        public async Task<string> PageSource(string frameName = null, int timeoutSeconds = 10)
        {
            if (webBrowser == null) return String.Empty;
            if (frameName == null) return await PageSource(webBrowser.GetBrowser().MainFrame, timeoutSeconds);
            return await PageSource(webBrowser.GetBrowser().GetFrame(frameName), timeoutSeconds);
        }*/

        public void WaitBrowserLoading(int timeoutSeconds)
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            int contLoading = 0;
            while (contLoading < timeoutSeconds && webBrowser.IsLoading)
            {
                Thread.Sleep(1000);
                contLoading++;
                //webBrowser.Reload();
            }
            if (contLoading >= timeoutSeconds)
            {
                throw new Exception("Tempo limite excedido");
            }
        }

        public bool IsLoading()
        {
            if (webBrowser == null) return false;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            return webBrowser.IsLoading;
        }

        public string Url()
        {
            if (webBrowser == null) return null;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            return webBrowser.Address;
        }

        public void RefreshContext()
        {
            if (webBrowser == null) return;
            webBrowser.RequestContext = new RequestContext();
        }

        public List<string> GetFrames()
        {
            if (webBrowser == null) return null;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            return webBrowser.GetBrowser().GetFrameNames();
        }

        public List<long> GetFramesId()
        {
            if (webBrowser == null) return null;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            return webBrowser.GetBrowser().GetFrameIdentifiers();
        }

        public void Close()
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            webBrowser.GetBrowser().Dispose();//CloseBrowser(true);
        }

        public void Reload()
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            webBrowser/*.GetBrowser()*/.Reload(true);
        }

        public bool NavigateBack()
        {
            if (webBrowser == null || !webBrowser.CanGoBack) return false;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            webBrowser.Back();
            return true;
        }

        public void Navigate(string url)
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            webBrowser.Load(url);
        }

        public void ClearAllCookies()
        {
            if (this.webBrowser != null)
            {

                try
                {
                    this.webBrowser.Reload();
                }
                catch { }
            }
            try
            {
                int result = Cef.GetGlobalCookieManager().DeleteCookiesAsync().Result;
            }
            catch { }
        }

        public void reloadCEF()
        {
            try
            {
                Navigate("about:blank");
            }
            catch { }
            // Reload ignore cache
            try
            {
                this.webBrowser.Reload();
            }
            catch { }
        }

        public object EvaluateScriptAsync(string script, string frameName = null, int timeoutSeconds = 10)
        {
            if (webBrowser == null) return null;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            Task<JavascriptResponse> task;
            if (frameName == null) task = webBrowser.GetBrowser().MainFrame.EvaluateScriptAsync(script);//, TimeSpan.FromSeconds(timeoutSeconds));
            else task = webBrowser.GetBrowser().GetFrame(frameName).EvaluateScriptAsync(script);//, TimeSpan.FromSeconds(timeoutSeconds));

            if (!task.IsCompleted)
            {
                try
                {
                    task.Start();
                }
                catch { }
                if (!task.Wait(TimeSpan.FromSeconds(timeoutSeconds)))
                {
                    try
                    {
                        webBrowser.Stop();
                    }
                    catch { }
                    int cont = 0;
                    while (webBrowser.IsLoading && cont < 10) // 10 segundos para interromper
                    {
                        Thread.Sleep(1000);
                        cont++;
                    }

                    if (webBrowser.IsLoading)
                        throw new Exception("Tempo limite atingido!");

                    // Tenta mais uma vez
                    if (!task.IsCompleted)
                    {
                        try
                        {
                            task.Start();
                        }
                        catch { }
                        if (!task.Wait(TimeSpan.FromSeconds(10))) // mais 10 segundos
                            throw new Exception("Tempo limite atingido!");
                    }
                }
            }

            JavascriptResponse response = task.Result;
            return response.Success ? (response.Result ?? "null") : response.Message;
        }

        public void ExecuteScriptAsync(string script, string frameName = null)
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            if (frameName == null) webBrowser.GetMainFrame().ExecuteJavaScriptAsync(script);
            else webBrowser.GetBrowser().GetFrame(frameName).ExecuteJavaScriptAsync(script);
        }

        public void ExecuteScriptAsync(string script, long frameIdentifier)
        {
            if (webBrowser == null) return;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            if (frameIdentifier < 0) webBrowser.ExecuteScriptAsync(script);
            else webBrowser.GetBrowser().GetFrame(frameIdentifier).ExecuteJavaScriptAsync(script);
        }

        public bool FrameIsFocused(string frameName = null)
        {
            if (webBrowser == null) return false;
            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");
            if (frameName == null) return webBrowser.GetBrowser().MainFrame.IsFocused;
            return webBrowser.GetBrowser().GetFrame(frameName).IsFocused;
        }

        public void WaitPageLoad(int timeoutSeconds, bool pooling, string[] urlsEsperadas, Action acao, string frameName, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            this.pageSource = null;
            this.xpathExpected = XPath;
            this.conditionExpected = condition;
            this.minElemExpected = minExpectedComponents;
            this.urlsEsperadas = urlsEsperadas;
            this.error = null;
            this.finish = false;
            wait = new Semaphore(0, 1);

            if (timer != null)
            {
                try
                {
                    timer.Dispose();
                }
                catch { }
            }

            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            BrowserLoading2 = (true);

            // Timer
            int time = timeoutSeconds > 0 ? timeoutSeconds : 10;
            timer = new System.Threading.Timer(o => Timeout(), null, time * 1000, time * 1000);

            if (!pooling) webBrowser.FrameLoadEnd += chromeBrowser_FrameLoadEnd;

            if (acao != null || /*webBrowser.IsLoading ||*/ pooling)
            {
                if (acao != null)
                {
                    try
                    {
                        webBrowser.Stop();
                    }
                    catch { }
                    int cont = 0;
                    while (webBrowser.IsLoading && cont < 10) // 10 segundos
                    {
                        Thread.Sleep(1000);
                        cont++;
                    }
                    acao.Invoke();
                }

                if (pooling)
                {
                    semaforo.WaitOne();
                    bool falhou = error != null;
                    semaforo.Release(1);
                    while (!falhou && (/*webBrowser.IsLoading ||*/ (urlsEsperadas != null && !urlsEsperadas.Any(t => webBrowser.Address.Contains(t)))))
                    {
                        Thread.Sleep(100);
                        semaforo.WaitOne();
                        falhou = error != null;
                        semaforo.Release(1);
                    }
                    semaforo.WaitOne();
                    finish = error == null;
                    semaforo.Release(1);
                }
                else wait.WaitOne();
            }

            semaforo.WaitOne();

            if (finish && xpathExpected != null)
            {
                //await PageSource(frameName);
                try
                {
                    PageSourceSync(frameName);
                }
                catch //(Exception e)
                {
                    //error = e.Message;
                }

                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                if (pageSource != null) document.LoadHtml(pageSource);
                HtmlNodeCollection collection = document.DocumentNode.SelectNodes(xpathExpected);
                while (error == null && (((collection == null || collection.Count == 0) && !condition.Equals(EXPECTEDCONDITION.INVISIBLE)) ||
                       (collection != null && collection.Count > 0 && (collection.Count < minElemExpected ||
                        (condition.Equals(EXPECTEDCONDITION.VISIBLE) && collection.First().GetAttributeValue("style", "").Contains("display: none")) ||
                        (condition.Equals(EXPECTEDCONDITION.INVISIBLE) && !collection.First().GetAttributeValue("style", "display: none").Contains("display: none"))))))
                {
                    finish = false;
                    semaforo.Release(1);
                    if (pooling)
                        Thread.Sleep(100);
                    else
                        wait.WaitOne();
                    semaforo.WaitOne();
                    //await PageSource(frameName);
                    try
                    {
                        PageSourceSync(frameName);
                    }
                    catch //(Exception e)
                    {
                        //error = e.Message;
                    }
                    if (pageSource != null) document.LoadHtml(pageSource);
                    if (error == null) collection = document.DocumentNode.SelectNodes(xpathExpected);
                }
                if (error == null) finish = true;
            }

            if (!pooling) webBrowser.FrameLoadEnd -= chromeBrowser_FrameLoadEnd;

            try
            {
                timer.Dispose();
            }
            catch { }
            timer = null;

            BrowserLoading2 = (false);

            if (error != null)
            {
                semaforo.Release(1);
                throw new Exception(error);
            }

            semaforo.Release(1);
            return;
        }

        public void WaitPageLoad(int timeoutSeconds)
        {
            WaitPageLoad(timeoutSeconds, false, null, null, null, null);
        }

        public void WaitPageLoad(int timeoutSeconds, Action acao)
        {
            WaitPageLoad(timeoutSeconds, false, null, acao, null, null);
        }

        public void WaitPageLoadPooling(int timeoutSeconds, string[] urlsEsperadas, Action acao)
        {
            WaitPageLoad(timeoutSeconds, true, urlsEsperadas, acao, null, null);
        }
        public void WaitPageLoadPooling(int timeoutSeconds, string[] urlsEsperadas, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, true, urlsEsperadas, null, null, XPath, condition, minExpectedComponents);
        }
        public void WaitPageLoadPooling(int timeoutSeconds, string[] urlsEsperadas, Action acao, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, true, urlsEsperadas, acao, null, XPath, condition, minExpectedComponents);
        }

        public void WaitPageLoadPooling(int timeoutSeconds, Action acao, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, true, null, acao, null, XPath, condition, minExpectedComponents);
        }
        public void WaitPageLoadPooling(int timeoutSeconds, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, true, null, null, null, XPath, condition, minExpectedComponents);
        }
        public void WaitPageLoadPooling(int timeoutSeconds, string frameName, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, true, null, null, frameName, XPath, condition, minExpectedComponents);
        }

        public void WaitPageLoad(int timeoutSeconds, string[] urlsEsperadas)
        {
            WaitPageLoad(timeoutSeconds, false, urlsEsperadas, null, null, null);
        }

        public void WaitPageLoad(int timeoutSeconds, string[] urlsEsperadas, Action action)
        {
            WaitPageLoad(timeoutSeconds, false, urlsEsperadas, action, null, null);
        }

        public void WaitPageLoad(int timeoutSeconds, string[] urlsEsperadas, Action action, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, false, urlsEsperadas, action, null, XPath, condition, minElemExpected);
        }

        public void WaitPageLoad(int timeoutSeconds, string[] urlsEsperadas, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, false, urlsEsperadas, null, null, XPath, condition, minExpectedComponents);
        }

        public void WaitPageLoad(int timeoutSeconds, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, false, null, null, null, XPath, condition, minExpectedComponents);
        }

        public void WaitPageLoad(int timeoutSeconds, Action acao, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, false, null, acao, null, XPath, condition, minExpectedComponents);
        }

        public void WaitPageLoad(int timeoutSeconds, Action acao, string frameName, string XPath, EXPECTEDCONDITION condition = EXPECTEDCONDITION.PRESENCE, int minExpectedComponents = 1)
        {
            WaitPageLoad(timeoutSeconds, false, null, acao, frameName, XPath, condition, minExpectedComponents);
        }

        private void Timeout()
        {
            try
            {
                timer.Dispose();
            }
            catch { }

            semaforo.WaitOne();
            if (finish || error != null)
            {
                semaforo.Release(1);
                return;
            }
            error = "Tempo limite atingido!";
            //webBrowser.FrameLoadEnd -= chromeBrowser_FrameLoadEnd;
            //webBrowser.LifeSpanHandler = null;
            //webBrowser.DownloadHandler = null;
            try
            {
                webBrowser.Stop();
            }
            catch { }
            semaforo.Release(1);
            try
            {
                wait.Release(1);
            }
            catch { }
        }

        public Process ProcessoCEF()
        {
            return processoCEF;
        }

        public void Stop()
        {
            //semaforo.WaitOne();
            error = "Navegação interrompida!";
            //webBrowser.FrameLoadEnd -= chromeBrowser_FrameLoadEnd;
            //webBrowser.LifeSpanHandler = null;
            //webBrowser.DownloadHandler = null;
            try
            {
                webBrowser.Stop();
            }
            catch { }
            //semaforo.Release(1);
            try
            {
                wait.Release(1);
            }
            catch { }
        }

        private void chromeBrowser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            ChromiumWebBrowser b = (ChromiumWebBrowser)sender;

            semaforo.WaitOne();
            if (error != null || (urlsEsperadas != null && !urlsEsperadas.Any(t => b.Address.Contains(t))))
            {
                semaforo.Release(1);
                return;
            }

            finish = true;

            semaforo.Release(1);

            try
            {
                wait.Release(1);
            }
            catch { }
        }

        //private void chromeBrowser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        //{
        //}

        public void ImpedeNovaJanelaAbrir(int timeoutSeconds)
        {
            wait = new Semaphore(0, 1);
            //loadingFile = false;
            this.error = null;
            this.finish = false;

            if (timer != null)
            {
                try
                {
                    timer.Dispose();
                }
                catch { }
            }

            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            BrowserLoading2 = (true);

            // Timer
            int time = timeoutSeconds > 0 ? timeoutSeconds : 10;
            timer = new System.Threading.Timer(o => Timeout(), null, time * 1000, time * 1000);

            webBrowser.LifeSpanHandler = new LifeSpanHandler(wait, false, false);

            wait.WaitOne();

            //webBrowser.LifeSpanHandler = null;
            webBrowser.LifeSpanHandler = new LifeSpanHandler(null, false, false);

            semaforo.WaitOne();

            try
            {
                timer.Dispose();
            }
            catch { }
            timer = null;

            BrowserLoading2 = (false);

            if (error != null)
            {
                semaforo.Release(1);
                throw new Exception(error);
            }

            semaforo.Release(1);
            return;
        }



        public bool DownloadFile(int timeoutSeconds, string directory, string fileName, Action acao, bool abreNovaJanela = false, Control formControl = null, Label txtStatus = null)
        {
            //this.file = null;

            wait = new Semaphore(0, 1);
            //loadingFile = false;
            this.error = null;
            this.finish = false;

            if (timer != null)
            {
                try
                {
                    timer.Dispose();
                }
                catch { }
            }

            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            BrowserLoading2 = (true);

            // Timer
            int time = timeoutSeconds > 0 ? timeoutSeconds : 10;
            timer = new System.Threading.Timer(o => Timeout(), null, time * 1000, time * 1000);


            if (abreNovaJanela)
            {
                webBrowser.LifeSpanHandler = new LifeSpanHandler(wait, true, true, new DownloadHandler(directory, fileName, wait, formControl, txtStatus));
                if (acao != null)
                {
                    try
                    {
                        webBrowser.Stop();
                    }
                    catch { }
                    int cont = 0;
                    while (webBrowser.IsLoading && cont < 10) // 10 segundos para interromper
                    {
                        Thread.Sleep(1000);
                        cont++;
                    }
                    acao.Invoke();
                }
                wait.WaitOne();

                Form control = webBrowser.FindForm();
                if (control != null)
                {
                    Form popup = webBrowser.LifeSpanHandler == null ? null : ((LifeSpanHandler)webBrowser.LifeSpanHandler).GetPopUpForm();
                    if (popup != null)
                    {
                        try
                        {
                            control.Invoke(new MethodInvoker(delegate ()
                            {
                                try
                                {
                                    popup.Close();
                                }
                                catch { }
                            }));
                        }
                        catch { }
                    }
                }

                //webBrowser.LifeSpanHandler = null;
                webBrowser.LifeSpanHandler = new LifeSpanHandler(null, false, false);
            }
            else
            {
                webBrowser.DownloadHandler = new DownloadHandler(directory, fileName, wait, formControl, txtStatus);

                if (!abreNovaJanela && acao != null) acao.Invoke();

                wait.WaitOne();

                webBrowser.DownloadHandler = null;
            }

            semaforo.WaitOne();

            try
            {
                timer.Dispose();
            }
            catch { }
            timer = null;

            BrowserLoading2 = (false);

            if (error != null)
            {
                semaforo.Release(1);
                return false;
            }

            semaforo.Release(1);
            return true;

        }

        public class DownloadHandler : IDownloadHandler
        {

            public event EventHandler<DownloadItem> OnBeforeDownloadFired;
            public event EventHandler<DownloadItem> OnDownloadUpdatedFired;
            private string directory;
            private string fileName;
            private Semaphore notify;
            private Label txtStatus;
            private Control formControl;
            public int count = 0;
            public DownloadHandler(string directory, string fileName, Semaphore semaforo, Control formControl, Label txtStatus)
            {
                this.directory = directory;
                this.notify = semaforo;
                this.fileName = fileName;
                this.formControl = formControl;
                this.txtStatus = txtStatus;
            }

            public string Directory()
            {
                return directory;
            }
            public string FileName()
            {
                return fileName;
            }

            public void OnBeforeDownload(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
            {
                Thread.Sleep(1000);
                if (!callback.IsDisposed)
                {
                    using (callback)
                    {
                        string arquivo = string.Empty;
                        string extensao = string.Empty;
                        int index = downloadItem.SuggestedFileName.LastIndexOf(".");
                        if (index > 0)
                        {
                            extensao = downloadItem.SuggestedFileName.Substring(index);
                            if (fileName == null)
                            {
                                arquivo = downloadItem.SuggestedFileName.Substring(0, index);
                            }
                            else
                            {
                                arquivo = fileName;
                            }
                        }
                        else
                        {
                            arquivo = fileName == null ? downloadItem.SuggestedFileName : fileName;
                        }
                        callback.Continue(directory + "\\" + arquivo + extensao, showDialog: false);
                    }
                }
            }

            public void OnDownloadUpdated(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
            {
                Thread.Sleep(1000);
                count++;
                Console.WriteLine(count);
                if (downloadItem.IsInProgress)
                {
                    if (formControl != null && txtStatus != null)
                    {
                        try
                        {
                            formControl.Invoke(new MethodInvoker(delegate ()
                            {
                                txtStatus.Text = "Download progress: " + (downloadItem.PercentComplete >= 0 ? downloadItem.PercentComplete : 0).ToString() + "%";
                            }));
                        }
                        catch { }
                    }
                    return;
                }

                if (downloadItem.IsComplete)
                {
                    if (downloadItem.IsValid)
                    {
                        if (formControl != null && txtStatus != null)
                        {
                            try
                            {
                                formControl.Invoke(new MethodInvoker(delegate ()
                                {
                                    txtStatus.Text = "Download progress: 100%";
                                }));
                            }
                            catch { }
                        }
                        notify.Release(1);
                    }
                }
            }
        }



        private void OpenNewWindow(int timeoutSeconds, bool fazerDownload, bool downloadAposAcessarJanela, string directory, string fileName, Action acao, int timeIfWindowOpens = 0, Control formControl = null, Label txtStatus = null)
        {
            //this.file = null;

            urlsEsperadas = null;
            xpathExpected = null;
            wait = new Semaphore(0, 1);
            //loadingFile = false;
            this.error = null;
            this.finish = false;

            if (timer != null)
            {
                try
                {
                    timer.Dispose();
                }
                catch { }
            }

            if (!ProcessoCEFExiste())
                throw new Exception("Processo CEF foi encerrado!");

            BrowserLoading2 = (true);

            // Timer
            int time = timeoutSeconds > 0 ? timeoutSeconds : 10;
            timer = new System.Threading.Timer(o => Timeout(), null, time * 1000, time * 1000);

            webBrowser.LifeSpanHandler = new LifeSpanHandler(wait);

            if (acao != null)
            {
                try
                {
                    webBrowser.Stop();
                }
                catch { }
                int cont = 0;
                while (webBrowser.IsLoading && cont < 10) // 10 segundos para interromper
                {
                    Thread.Sleep(1000);
                    cont++;
                }
                acao.Invoke();
            }

            wait.WaitOne();

            string url = null;
            if (error == null)
            {
                //do
                //{
                //	if (!Program.MODO_DESENVOLVEDOR || !Program.IGNORE_DELAY) Thread.Sleep(Program.DELAY_THREAD);
                //	url = ((LifeSpanHandler)webBrowser.LifeSpanHandler).GetUrl();
                //}
                //while (url == null || url.Trim().Equals(""));
            }
            //string url = error == null && webBrowser.LifeSpanHandler != null ? ((LifeSpanHandler)webBrowser.LifeSpanHandler).GetUrl() : null;

            //webBrowser.LifeSpanHandler = null;
            webBrowser.LifeSpanHandler = new LifeSpanHandler(null, false, false);

            if (timeIfWindowOpens > time)
            {
                if (timer != null)
                {
                    try
                    {
                        timer.Dispose();
                    }
                    catch { }
                }
                timer = new System.Threading.Timer(o => Timeout(), null, (timeIfWindowOpens - time) * 1000, (timeIfWindowOpens - time) * 1000);
            }

            if (url != null)
            {
                if (fazerDownload)// && !downloadAposAcessarJanela)
                {
                    string arquivo = string.Empty;
                    string extensao = string.Empty;
                    int index = url.LastIndexOf(".");
                    int indexB = url.LastIndexOf("/");
                    if (index > 0 && index > indexB)
                    {
                        extensao = url.Substring(index);
                        if (fileName == null)
                            arquivo = url.Substring(indexB > 0 ? indexB : 0, index);
                        else
                            arquivo = fileName;
                    }
                    else
                    {
                        arquivo = fileName == null ? url.Substring(indexB > 0 ? indexB : 0) : fileName;
                    }

                    WebClient webClient = new WebClient();
                    try
                    {
                        webClient.DownloadFile(url, directory + "\\" + arquivo + extensao);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.InnerException == null ? e.Message : e.InnerException.InnerException == null ? e.InnerException.Message : e.InnerException.InnerException.Message);
                    }
                }
                else if (timeIfWindowOpens > time)
                {
                    // Acessa a página
                    urlsEsperadas = new string[] { url };
                    webBrowser.FrameLoadEnd += chromeBrowser_FrameLoadEnd;
                    webBrowser.Load(url);
                    wait.WaitOne();
                    webBrowser.FrameLoadEnd -= chromeBrowser_FrameLoadEnd;

                    //if (fazerDownload && downloadAposAcessarJanela)
                    //{
                    //    webBrowser.DownloadHandler = new DownloadHandler(directory, fileName, wait, formControl, txtStatus);
                    //    wait.WaitOne();
                    //    webBrowser.DownloadHandler = null;
                    //    semaforo.WaitOne();
                    //    bool falhou = error != null;
                    //    semaforo.Release(1);
                    //    if (!falhou)
                    //    {
                    //        webBrowser.FrameLoadEnd += chromeBrowser_FrameLoadEnd;
                    //        NavigateBack();
                    //        wait.WaitOne();
                    //        webBrowser.FrameLoadEnd -= chromeBrowser_FrameLoadEnd;
                    //    }
                    //}
                }
            }

            semaforo.WaitOne();

            try
            {
                timer.Dispose();
            }
            catch { }
            timer = null;

            BrowserLoading2 = (false);

            if (error != null)
            {
                semaforo.Release(1);
                throw new Exception(error);
            }

            semaforo.Release(1);
            return;

        }

        public void OpenNewWindowPage(int timeoutSeconds, Action acao, int timeIfWindowOpens = 0)
        {
            OpenNewWindow(timeoutSeconds, false, false, null, null, acao, timeIfWindowOpens);
        }

        public void OpenNewWindowDownload(int timeoutSeconds, string directory, string fileName, Action acao, int timeIfWindowOpens = 0, Control formControl = null, Label txtStatus = null)
        {
            OpenNewWindow(timeoutSeconds, true, false, directory, fileName, acao, timeIfWindowOpens, formControl, txtStatus);
        }

        //public void DownloadFileAfterOpenNewWindow(int timeoutSeconds, string directory, string fileName, Action acao, int timeIfWindowOpens = 0, Control formControl = null, Label txtStatus = null)
        //{
        //    OpenNewWindow(timeoutSeconds, true, true, directory, fileName, acao, timeIfWindowOpens, formControl, txtStatus);
        //}


        public List<CefSharp.Cookie> GetCookies(int timeoutSeconds = 10)
        {
            if (webBrowser == null) return null;
            string url = Url();
            //CookieVisitor visitor = new CookieVisitor();
            Task<List<CefSharp.Cookie>> task = Cef.GetGlobalCookieManager().VisitUrlCookiesAsync(url, true);

            if (!task.IsCompleted)
            {
                if (!task.Status.Equals(TaskStatus.Running))
                {
                    try
                    {
                        task.Start();
                    }
                    catch { }
                }
                if (!task.Wait(TimeSpan.FromSeconds(timeoutSeconds)))
                {
                    throw new Exception("Tempo limite atingido!");
                }
            }

            return task.Result;
        }

        //internal class CookieVisitor : ICookieVisitor
        //{
        //    private List<CefSharp.Cookie> Cookies { get; set; }

        //    public CookieVisitor()
        //    {
        //        this.Cookies = new List<CefSharp.Cookie>();
        //    }


        //    public bool Visit(CefSharp.Cookie cookie, int count, int total, ref bool deleteCookie)
        //    {
        //        CefSharp.Cookie ck = new CefSharp.Cookie();
        //        ck.Creation = Convert.ToDateTime(cookie.Creation);
        //        ck.Domain = cookie.Domain;
        //        ck.Expires = cookie.Expires == null ? (DateTime?)null : Convert.ToDateTime(cookie.Expires.Value);
        //        ck.HttpOnly = cookie.HttpOnly;
        //        ck.LastAccess = Convert.ToDateTime(cookie.LastAccess);
        //        ck.Name = cookie.Name;
        //        ck.Path = cookie.Path;
        //        ck.Secure = cookie.Secure;
        //        ck.Value = cookie.Value;
        //        this.Cookies.Add(ck);

        //        deleteCookie = false;
        //        return true;
        //    }

        //    public void Dispose()
        //    {

        //    }
        //}



        public class LifeSpanHandler : ILifeSpanHandler
        {
            private Semaphore notify;
            private string targetUrl;
            private bool allowWindow;
            private bool redirecionar;
            private IDownloadHandler downloadHandler;
            private Form popup;

            public LifeSpanHandler(Semaphore notify, bool allowWindow = false, bool redirecionar = true, IDownloadHandler downloadHandler = null)
            {
                this.notify = notify;
                this.targetUrl = null;
                this.allowWindow = allowWindow;
                this.redirecionar = redirecionar;
                this.downloadHandler = downloadHandler;
                this.popup = null;
            }

            public string GetUrl()
            {
                return targetUrl;
            }

            public Form GetPopUpForm()
            {
                return popup;
            }

            bool ILifeSpanHandler.OnBeforePopup(IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
            {
                if (!allowWindow)
                {
                    // não abre novo browser
                    newBrowser = null;
                    if (redirecionar)
                    {
                        //if (targetUrl.Equals("about:blank")) return true;
                        this.targetUrl = targetUrl;
                    }
                    if (notify != null) notify.Release(1);
                    return true;
                }


                ChromiumWebBrowser chromiumWebBrowser = (ChromiumWebBrowser)browserControl;

                ChromiumWebBrowser newChromeBrowser = null;

                chromiumWebBrowser.Invoke(new Action(() =>
                {
                    Form owner = chromiumWebBrowser.FindForm();
                    newChromeBrowser = new ChromiumWebBrowser(targetUrl)
                    {
                        DownloadHandler = downloadHandler,
                        //LifeSpanHandler = new LifeSpanHandler(null),
                        Dock = DockStyle.Fill
                    };
                    newChromeBrowser.SetAsPopup();

                    if (popup == null)
                    {
                        popup = new Form
                        {
                            MaximizeBox = false,
                            MinimizeBox = false,
                            Left = 10000,
                            Top = 10000,
                            StartPosition = FormStartPosition.Manual,
                            Width = 1,
                            Height = 1,
                        };
                        popup.Shown += popup_Shown;

                        owner.AddOwnedForm(popup);
                    }
                    else if (popup.Controls.Count > 0) popup.Controls.RemoveAt(0);

                    popup.Controls.Add(newChromeBrowser);

                    popup.Show();
                }));

                newBrowser = newChromeBrowser;

                return false;
            }

            private void popup_Shown(object sender, EventArgs e)
            {
                if (popup == null) return;
                Form owner = popup.FindForm();
                if (owner != null) owner.BringToFront();
                popup.WindowState = FormWindowState.Minimized;
                popup.ShowInTaskbar = false;
                popup.Hide();
            }

            void ILifeSpanHandler.OnAfterCreated(IWebBrowser browserControl, IBrowser browser)
            {
            }

            bool ILifeSpanHandler.DoClose(IWebBrowser browserControl, IBrowser browser)
            {
                return false;
            }

            void ILifeSpanHandler.OnBeforeClose(IWebBrowser browserControl, IBrowser browser)
            {

            }
        }



        public class KeyboardHandler : IKeyboardHandler
        {
            public bool OnKeyEvent(IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey)
            {
                return true;
            }

            public bool OnPreKeyEvent(IWebBrowser browserControl, IBrowser browser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey, ref bool isKeyboardShortcut)
            {
                return true;
            }
        }

        public class JsHandler : IJsDialogHandler
        {
            public bool OnJSDialog(IWebBrowser browserControl, IBrowser browser, string originUrl, CefJsDialogType dialogType, string messageText, string defaultPromptText, IJsDialogCallback callback, ref bool suppressMessage)
            {
                suppressMessage = false;
                ChromiumWebBrowser chromiumBrowser = (ChromiumWebBrowser)browserControl;
                try
                {
                    chromiumBrowser.ExecuteScriptAsync("var s = document.createElement('script');" +
                                                       //"s.type = 'text/javascript';" +
                                                       "s.innerHTML = fakeAlert('" + messageText.Replace("'", "") + "');" +
                                                       "document.body.appendChild(s)");
                    Thread.Sleep(500);
                }
                catch { }
                return true;
            }

            public bool OnJSBeforeUnload(IBrowser browser, string message, bool isReload, IJsDialogCallback callback)
            {
                return true;
            }

            public void OnResetDialogState(IWebBrowser browserControl, IBrowser browser)
            {
            }

            public void OnDialogClosed(IWebBrowser browserControl, IBrowser browser)
            {
            }

            public bool OnBeforeUnloadDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, string message, bool isReload, IJsDialogCallback callback)
            {
                return true;
            }
        }

        public class DialogHandler : IDialogHandler
        {
            public bool OnFileDialog(IWebBrowser chromiumWebBrowser, IBrowser browser, CefFileDialogMode mode, CefFileDialogFlags flags, string title, string defaultFilePath, List<string> acceptFilters, int selectedAcceptFilter, IFileDialogCallback callback)
            {
                return false;
            }
        }


    }
}
