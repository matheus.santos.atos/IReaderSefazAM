﻿using AtosCapital;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace IReaderSEFAZAM.Classes
{
    public class Email
    {
        private const byte CATALOGO_IREADER = 28;
        private AppSettingsReader str = new AppSettingsReader();
        public void EnviarEmail(string assunto, string mensagem)
        {
            try
            {
                using (BancoDados _db = new BancoDados())
                {
                    string content = "{ \"titulo\" : \"" + assunto.Replace("'", "''").Replace("\\", "\\\\").Replace("\"", "\\\"") + "\"" +
                             ", \"mensagem\": \"" + mensagem.Replace("'", "''").Replace("\\", "\\\\").Replace("\"", "\\\"") +
                             "\" }";

                    _db.ExecuteScript("admin.tbnews", (int?)null, "INSERT INTO admin.tbNews (dsNews, dtNews, cdEmpresaGrupo, cdCatalogo, cdCanal, dsReporter)" +
                                      " VALUES('" + content + "', getDate(), NULL, " + CATALOGO_IREADER + ", 2, 'iReaderSP')");
                }

                ////return;
                //string EmailSender = (string)str.GetValue("EmailSender", typeof(string));
                //string EmailSenderSenha = (string)str.GetValue("EmailSenderSenha", typeof(string));
                //string EmailPara = (string)str.GetValue("EmailPara", typeof(string));
                //string EmailComCopia = (string)str.GetValue("EmailComCopia", typeof(string));
                //string EmailComCopia1 = (string)str.GetValue("EmailComCopia1", typeof(string));
                //string EmailComCopia2 = (string)str.GetValue("EmailComCopia2", typeof(string));
                //string EmailComCopia3 = (string)str.GetValue("EmailComCopia3", typeof(string));
                //string EmailSenderNome = (string)str.GetValue("EmailSenderNome", typeof(string));

                //MailMessage objEmail = new MailMessage();
                //objEmail.From = new MailAddress(EmailSender, EmailSenderNome);
                ////objEmail.ReplyTo = "";
                //objEmail.To.Add(EmailPara);
                //objEmail.CC.Add(EmailComCopia);
                //objEmail.CC.Add(EmailComCopia1);
                //objEmail.CC.Add(EmailComCopia2);
                //objEmail.CC.Add(EmailComCopia3);
                ////objEmail.Bcc.Add("Email oculto");
                //objEmail.Priority = MailPriority.High;
                //objEmail.IsBodyHtml = true;
                //objEmail.Subject = assunto;
                //objEmail.Body = mensagem;
                //objEmail.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
                //objEmail.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");
                //SmtpClient objSmtp = new SmtpClient();
                //objSmtp.Host = "smtp.gmail.com";
                //objSmtp.EnableSsl = true;
                //objSmtp.Port = 587;
                //objSmtp.UseDefaultCredentials = false;
                //objSmtp.Credentials = new NetworkCredential(EmailSender, EmailSenderSenha);
                //objSmtp.Send(obj);
            }
            catch (Exception ex)
            {

            }
        }

        private StringBuilder strMensagemJob(string Acao)
        {
            try
            {
                StringBuilder stb = new StringBuilder();
                stb.AppendLine("<!DOCTYPE html>");
                stb.AppendLine("<html>");
                stb.AppendLine("<head>");
                stb.AppendLine("<title>Page Title</title>");
                stb.AppendLine("<style>");
                stb.AppendLine("body {");
                stb.AppendLine("background-color: white;");
                stb.AppendLine("  text-align: center;");
                stb.AppendLine("  color: gray;");
                stb.AppendLine("  font-family: Arial, Helvetica, sans-serif;");
                stb.AppendLine("}");
                stb.AppendLine("");
                stb.AppendLine("</style>");
                stb.AppendLine("");
                stb.AppendLine("</head>");
                stb.AppendLine("<body>");
                stb.AppendLine("<h1>Informativo Job Atos Capital</h1>");
                stb.AppendLine("<p>[MENSAGEM ENVIADA AUTOM&Aacute;TICAMENTE PELO JOB MONITOR DA ATOS]</p>");
                stb.AppendLine("<p><span style=\"color:#3366ff;\"><strong>Olá</strong></span>,</p>");
                stb.AppendLine("<p>&nbsp;</p>");
                stb.AppendLine("<p>O Job Monitor de Eventos acabou de <strong>" + Acao + "</strong> o LeitorXML, o mesmo será reiniciado em cinco minutos: <br/>");
                stb.AppendLine("<p>&nbsp;</p>");
                stb.AppendLine("<br/>");
                stb.AppendLine("<p style=\"align=left;\">Atos Capital.</p>");
                stb.AppendLine("</body>");
                stb.AppendLine("</html>");

                return stb;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
