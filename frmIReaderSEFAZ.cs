﻿using AtosCapital;
using CefSharp;
using CefSharp.WinForms;
using HtmlAgilityPack;
using IReaderSEFAZAM.Classes;
using IReaderSEFAZAM.SEFAZ;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace IReaderSEFAZAM
{
    public partial class frmIReaderSEFAZ : Form
    {
        private ChromiumWebBrowser webBrowserI;
        private BrowserChrome browser;
        private string url = "";
        private string path;
        private bool IsExecucao = false;
        public List<string> ListDirectory = null;
        private string connetionString;
        public int total;
        public int atual;
        public string QUEUE_DIRECTORY = AppDomain.CurrentDomain.BaseDirectory + "ArquivosTXT";
        public string PATH_CERTIFICADO = AppDomain.CurrentDomain.BaseDirectory + @"Certificados";
        public string ARQUIVO_TEXTO = string.Empty;
        public string[] ARQUIVOS = new string[] { "notas" };
        private tbNotaFiscalLog NotaFiscalLog;
        public long idNotaFiscalLogDetalhe;

        public delegate void EventEscreveArquivoTexto(string arquivo, Dictionary<string, string> dicionario);
        public delegate void EventEscreveArquivoTextoLista<T>(string arquivo, List<T> list, int start, bool serializavel = false, bool overrideFile = false);
        public delegate void EventPreencheArquivoTextoLista<T>(string arquivo, List<T> list, bool serializavel = false);

        public frmIReaderSEFAZ()
        {
            InitializeComponent();
        }

        public string RemoveDiacritics(string text)
        {
            string normalizedString = text.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            foreach (char c in normalizedString)
            {
                UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public void setAtividade(string text)
        {
            this.Invoke(new MethodInvoker(delegate ()
            {
                lbAtividade.Text = text;
            }));
        }
        public string getdtInicial()
        {

            string dtInicial = "";
            this.Invoke(new MethodInvoker(delegate ()
            {
                dtInicial = lbDtInicio.Text;
            }));
            return dtInicial;
        }
        public string getdtFinal()
        {

            string dtfinal = "";
            this.Invoke(new MethodInvoker(delegate ()
            {
                dtfinal = lbDtFim.Text;
            }));
            return dtfinal;
        }

        public string getlbConnectString()
        {

            string certificadoSefazAM = "";
            this.Invoke(new MethodInvoker(delegate ()
            {
                certificadoSefazAM = lbConnectString.Text;
            }));
            return certificadoSefazAM;
        }
        public string getlbNomeProcesso()
        {

            string lbNP = "";
            this.Invoke(new MethodInvoker(delegate ()
            {
                lbNP = lbNomeProcesso.Text;
            }));
            return lbNP;
        }

        public bool soLerNotas()
        {
            bool retorno = false;
            this.Invoke(new MethodInvoker(delegate ()
            {
                retorno = cbSoLerNotas.Checked;
            }));
            return retorno;
        }
        public bool soBaixarNotas()
        {
            bool retorno = false;
            this.Invoke(new MethodInvoker(delegate ()
            {
                retorno = cbSoBaixar.Checked;
            }));
            return retorno;
        }

        public void setsoBaixarNotas(bool check)
        {
            this.Invoke(new MethodInvoker(delegate ()
            {
                cbSoBaixar.Checked = check;
            }));
        }

        #region Configuracao
        public void Configuracao()//(string Processo, string certificado, string dtInicio, string dtFim, string connetionString, string soBaixar, string soLer)
        {
            try
            {

                if (!Directory.Exists(PATH_CERTIFICADO))
                {
                    Directory.CreateDirectory(PATH_CERTIFICADO);
                }

                using (BancoDados _dbAtos = new BancoDados(true))
                //script para consulta das cargas
                {
                    List<tbNotaFiscalLog> tbNotasFiscaisLog = BancoDadosUtils.ConvertDataTableToDataType<tbNotaFiscalLog>(
                                                                                          _dbAtos.SqlQueryDataTable(@"select M.*,C.dsSefaz from nfce.tbNotaFiscalLog M (NOLOCK)
                                                                                                JOIN nfce.tbCertificado C (NOLOCK) ON(C.idCertificado = M.idCertificado)
                                                                                                WHERE M.dtExecucao <= getDate() and M.dtFinalizado is null and C.dsSefaz = 'SEFAZAM'
                                                                                                ORDER BY M.dtExecucao")
                                                                                                                                                                                        ).ToList();



                    if (tbNotasFiscaisLog.Count > 0)
                    {
                        NotaFiscalLog = tbNotasFiscaisLog.FirstOrDefault();

                        #region Busca certificado e gera na pasta de certificados
                        DataTable dt = _dbAtos.SqlQueryDataTable("select * from nfce.tbCertificado (nolock) where idCertificado = " + NotaFiscalLog.idCertificado);
                        string nomeArquivoSaida = dt.Rows[0]["dsNomeEmpresa"].ToString() + "-" +
                                                  dt.Rows[0]["dsSenha"].ToString() + "-" +
                                                  dt.Rows[0]["dsSefaz"].ToString() + ".pfx";
                        byte[] saida = (byte[])dt.Rows[0]["dsArquivo"];
                        string pathSaida = Path.Combine(PATH_CERTIFICADO, nomeArquivoSaida);
                        if (!File.Exists(pathSaida))
                            File.Create(pathSaida).Close();
                        else
                        {
                            File.Delete(pathSaida);
                            File.Create(pathSaida).Close();
                        }

                        File.WriteAllBytes(pathSaida, saida);
                        #endregion
                    }
                    else
                    {
                        Program.FinishApp();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Program.FinishApp();
                return;
            }
            string Processo = RemoveDiacritics(NotaFiscalLog.dscertificado).ToString().Replace(".pfx", "") + "_" + NotaFiscalLog.nrCNPJ + "_" + NotaFiscalLog.dtInicio.ToString("dd/MM/yyyy").Replace("/", "-") + "_" + NotaFiscalLog.dtFim.ToString("dd/MM/yyyy").Replace("/", "-") + "_" + (NotaFiscalLog.flSoBaixar == null || NotaFiscalLog.flSoBaixar == false ? "0" : "1") + "_" + (NotaFiscalLog.flSoLer == null || NotaFiscalLog.flSoLer == false ? "0" : "1");
            ListDirectory = new List<string> { Path.Combine(QUEUE_DIRECTORY, Processo, @"Arquivos") };
            //caminho = AppDomain.CurrentDomain.BaseDirectory + Processo + @"\Arquivos";
            ListDirectory.Add(Path.Combine(QUEUE_DIRECTORY, Processo));
            CreateDirectory();
            this.Invoke(new MethodInvoker(delegate ()
            {
                lbNomeProcesso.Text = Processo;
                this.connetionString = NotaFiscalLog.dscertificado;
                lbConnectString.Text = NotaFiscalLog.dscertificado;

                lbDtInicio.Text = NotaFiscalLog.dtInicio.ToString("dd/MM/yyyy");

                lbDtFim.Text = NotaFiscalLog.dtFim.ToString("dd/MM/yyyy");

                cbSoBaixar.Checked = NotaFiscalLog.flSoBaixar == null ? false : (bool)NotaFiscalLog.flSoBaixar;

                cbSoLerNotas.Checked = NotaFiscalLog.flSoLer == null ? false : (bool)NotaFiscalLog.flSoLer;

                ARQUIVO_TEXTO = Processo;
            }));

        }
        #endregion Configuracao

        #region ConfigureCEF
        private void ConfigureCEF()
        {
            //Configurações para o CEF# para simular um navegador integrado ao formulário dentro da aplicação
            CefSettings settings = new CefSettings();
            settings.CachePath = null;
            settings.PersistSessionCookies = false;
            settings.CefCommandLineArgs.Add("disable-application-cache", "1");
            settings.CefCommandLineArgs.Add("disable-session-storage", "1");
            settings.BrowserSubprocessPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                                                  //Environment.Is64BitProcess ? "x64" : "x86",
                                                  "CefSharp.BrowserSubprocess.exe");
            Cef.Initialize(settings, performDependencyCheck: false, browserProcessHandler: null);
            //Cef.GetGlobalCookieManager().SetStoragePath(COOKIES_PATH, false);
            //Cef.GetGlobalCookieManager().DeleteCookies("", "");
        }
        #endregion

        #region LoadBrowser
        private void LoadBrowser()
        {
            //utiliza o cef# para carregar o navegador

            //ConfigureCEF();
            string dir = AppDomain.CurrentDomain.BaseDirectory;
            var missingDeps = CefSharp.DependencyChecker.CheckDependencies(true, false, dir, string.Empty,
                Path.Combine(dir, "CefSharp.BrowserSubprocess.exe"));

            webBrowserI = new ChromiumWebBrowser(@"https://www.google.com");
            webBrowserI.Name = "WebBrowserI";
            webBrowserI.TabIndex = 0;
            webBrowserI.BrowserSettings.AcceptLanguageList = "{pt-BR}";
            webBrowserI.BrowserSettings.ApplicationCache = CefState.Disabled; // desabilita a cache
            webBrowserI.BrowserSettings.FileAccessFromFileUrls = CefState.Enabled;
            webBrowserI.BrowserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            webBrowserI.BrowserSettings.ImageLoading = CefState.Enabled;
            webBrowserI.BrowserSettings.Javascript = CefState.Enabled;
            webBrowserI.BrowserSettings.WebSecurity = CefState.Disabled;
            webBrowserI.BrowserSettings.WebGl = CefState.Disabled;
            webBrowserI.BrowserSettings.LocalStorage = CefState.Disabled;


            //webBrowserI.RequestHandler = new BrowserChrome.RequestHandler(); // locale
            webBrowserI.KeyboardHandler = new BrowserChrome.KeyboardHandler(); // não permite interação do teclado
            webBrowserI.JsDialogHandler = new BrowserChrome.JsHandler(); // sem alertas na página
            //webBrowserI.DialogHandler = new BrowserChrome.DialogHandler(); // sem alertas na página
            webBrowserI.ConsoleMessage += (sender, args) =>
            {
                string message = string.Format("WebviewI {0}({1}): {2}", args.Source, args.Line, args.Message);
                //currentLogBrowser += message + "\n";                  
            };


            // Adiciona na tab page
            TbBrowser.Controls.Add(webBrowserI); // TabPage tabBrowserI

            // Preenche a tabpage toda
            webBrowserI.Dock = DockStyle.Fill;
            TbBrowser.Show();

        }
        #endregion

        #region Criar Diretorios de configurações se não existir
        private void CreateDirectory()
        {

            foreach (string path in ListDirectory)
            {
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch { }
                }
            }

            if (!Directory.Exists(QUEUE_DIRECTORY))
            {
                try
                {
                    Directory.CreateDirectory(QUEUE_DIRECTORY);
                }
                catch { }
            }
        }
        #endregion

        #region start
        private void Start()
        {
            Thread.Sleep(5000);
            Email email = new Email();
            try
            {
                if (!IsExecucao)
                {
                    IsExecucao = true;
                    //metodo similar ao setState() do react para mudar qualquer coisa do formulário
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        lbStatus.Text = "EM EXECUÇÃO";
                    }));

                    //cada carga é composta de 1 dia e 1 cnpj
                    #region Cria o NotaFiscalLogDetalhe
                    try
                    {
                        using (BancoDados _dbAtos = new BancoDados(true))
                        {

                            idNotaFiscalLogDetalhe = _dbAtos.ExecuteScriptScalar("nfce.tbNotaFiscalLogDetalhe", (int?)null,
                                                @"INSERT INTO nfce.tbNotaFiscalLogDetalhe(idNotaFiscalLog,dtExecucaoIni)
                                                Values(" + NotaFiscalLog.idNotaFiscalLog + ",GETDATE()); SELECT SCOPE_IDENTITY() AS idNotaFiscalLogDetalhe");


                        }
                    }
                    catch (Exception exx)
                    {
                        throw new Exception("Não foi possível criar o NotaFiscalLogDetalhe " + exx.Message);
                    }
                    #endregion

                    //utiliza o Xpath para acessar o elemento diretamente
                    #region CARREGAR Notas
                    if (!browser.Esperar2("//*[@class='gLFyf gsfi']", null, 30))
                        throw new Exception("Elemento hplogo não encontrado!");

                    switch (NotaFiscalLog.dsSefaz)
                    {
                        case "SEFAZAM":
                            SEFAZAM sefazAM = new SEFAZAM(this, PATH_CERTIFICADO, browser, webBrowserI, NotaFiscalLog);
                            sefazAM.LerSefazAM();
                            break;
                    }


                    RemoverCertificado();
                    #endregion
                    IsExecucao = false;
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        lbStatus.Text = "PARADO";
                    }));
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        this.Close();
                    }));
                }
            }
            catch (Exception ex)
            {
                
                try
                {
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        lbStatus.Text = "PARADO";
                    }));
                    this.Invoke(new MethodInvoker(delegate ()
                    {
                        this.Close();
                    }));
                }
                catch { }

            }
        }
        #endregion

        #region Carregar
        public void Carregar()
        {
            System.Threading.ThreadStart ts = new System.Threading.ThreadStart(Start);
            System.Threading.Thread t = new System.Threading.Thread(ts);
            t.IsBackground = true;
            t.Start();
        }
        #endregion

        #region ARQUIVOS TEXTOS
        public string getConteudoArquivoTexto(string arquivo)
        {
            if (!Directory.Exists(QUEUE_DIRECTORY))
            {
                try
                {
                    Directory.CreateDirectory(QUEUE_DIRECTORY);
                }
                catch //(Exception e)
                {
                    // falha...
                    return String.Empty;
                }
            }
            string path = QUEUE_DIRECTORY + "\\" + ARQUIVO_TEXTO + "_" + arquivo + ".txt";
            if (File.Exists(path))
            {
                try
                {
                    return File.ReadAllText(path, Encoding.UTF8);
                }
                catch//(Exception e)
                {
                    return String.Empty;
                }
            }
            //else File.WriteAllText(path, String.Empty);
            return String.Empty;
        }

        public void escreveArquivoTexto(string arquivo, Dictionary<string, string> dicionario)
        {
            if (dicionario == null) return;
            string path = this.GetCaminho()+ "\\" + ARQUIVO_TEXTO + "_" + arquivo + ".txt";
            string conteudo = string.Join(";", dicionario.Select(x => x.Key + "=" + x.Value).ToArray());//.Replace(";", ";" + Environment.NewLine);
            try
            {
                File.WriteAllText(path, conteudo, Encoding.UTF8);
            }
            catch //(Exception e)
            {
                //Console.WriteLine(e.Message);
            }
        }

        public void escreveArquivoTexto(string arquivo, DataTable tabela, int start, bool overrideFile = false, bool csv = false)
        {
            if (start < 0 || (overrideFile && start != 0) || (!overrideFile && tabela.Rows.Count <= start))
                return;

            string path = QUEUE_DIRECTORY + "\\" + ARQUIVO_TEXTO + "_" + arquivo + (csv ? ".csv" : ".txt");

            try
            {
                if (csv)
                {
                    string content = string.Empty;
                    //if (MODO_DESENVOLVEDOR)
                    //    content = string.Join(Environment.NewLine, tabela.AsEnumerable().Skip(start).Select(t => string.Join(";", t.ItemArray.Select(x => x.ToString().Replace(Environment.NewLine, " ").Replace("\r", "").Replace("\n", " "))))) + Environment.NewLine;
                    //else
                    //{
                    for (int i = start; i < tabela.Rows.Count; i++)
                    {
                        Thread.Sleep(1);

                        DataRow row = tabela.Rows[i];
                        object[] array = row.ItemArray;
                        int k = 0;
                        for (k = 0; k < array.Length - 1; k++)
                        {
                            //Thread.Sleep(1);

                            content += array[k].ToString().Replace(Environment.NewLine, " ").Replace("\r", "").Replace("\n", " ") + ";";
                        }
                        content += array[k] + Environment.NewLine;
                    }
                    //}
                    if (!overrideFile && File.Exists(path))
                        File.AppendAllText(path, content, Encoding.UTF8);
                    else
                        File.WriteAllText(path, content, Encoding.UTF8);
                }
                else
                {
                    tabela.TableName = arquivo;
                    tabela.WriteXml(path);
                }
            }
            catch //(Exception e)
            {
                //Console.WriteLine(e.Message);
            }
        }

        public void escreveArquivoTexto<T>(string arquivo, List<T> list, int start, bool serializavel = false, bool overrideFile = false)
        {
            if (start < 0 || (overrideFile && start != 0) || (!overrideFile && list.Count <= start))
                return;

            string path = QUEUE_DIRECTORY + "\\" + ARQUIVO_TEXTO + "_" + arquivo + ".txt";
            string content = String.Empty;

            //if (arquivo.Equals("acessosList"))
            //    File.WriteAllText(QUEUE_DIRECTORY + "\\" + ARQUIVO_TEXTO + "_" + arquivo + "-T.txt", "COUNT : " + list.Count + " TYPE " + list.GetType().ToString());

            //if (MODO_DESENVOLVEDOR)
            //{
            //    if (serializavel)
            //        content = string.Join(Environment.NewLine, list.Skip(start).Select(t => JsonConvert.SerializeObject(t))) + Environment.NewLine;
            //    else
            //        content = string.Join(Environment.NewLine, list.Skip(start).Select(t => t.ToString().Replace(Environment.NewLine, " ").Replace("\r", "").Replace("\n", " "))) + Environment.NewLine;
            //}
            //else
            //{
            //ArquivoTexto.EscreveArquivo(path, arquivo, list, start, serializavel, overrideFile);
            //return;
            for (int k = start; k < list.Count; k++)
            {
                Thread.Sleep(1);

                string conteudo = String.Empty;
                if (!serializavel)
                    conteudo = list[k].ToString().Replace(Environment.NewLine, " ").Replace("\r", "").Replace("\n", " ");
                else
                {
                    //conteudo = ArquivoTexto.SerializaObjeto(list[k]);
                    conteudo = JsonConvert.SerializeObject(list[k]);
                }
                content += conteudo + Environment.NewLine;
            }
            //}

            try
            {
                if (!overrideFile && File.Exists(path))
                    File.AppendAllText(path, content, Encoding.UTF8);
                else
                    File.WriteAllText(path, content, Encoding.UTF8);
            }
            catch //(Exception e)
            {
                //Console.WriteLine(e.Message);
            }
        }

        public void preencheListFromArquivoTexto<T>(string arquivo, List<T> list, bool serializavel = false)
        {
            string content = getConteudoArquivoTexto(arquivo);

            if (content.Trim().Equals("")) return;

            string[] registros = content.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

            //if (MODO_DESENVOLVEDOR)
            //{
            //    if (serializavel)
            //        list.AddRange(registros.Where(t => !t.Trim().Equals("")).Select(t => JsonConvert.DeserializeObject<T>(t)));
            //    else
            //        list.AddRange(registros.Where(t => !t.Trim().Equals("")).Select(t => (T)Convert.ChangeType(t, typeof(T))));
            //}
            //else
            //{
            for (int k = 0; k < registros.Length; k++)
            {
                Thread.Sleep(1);

                string registro = registros[k];
                if (!registro.Trim().Equals(""))
                {
                    if (!serializavel) list.Add((T)Convert.ChangeType(registro, typeof(T)));
                    else
                    {
                        T item = JsonConvert.DeserializeObject<T>(registro);
                        list.Add(item);
                    }
                }
            }
            //}
        }

        protected void preencheDataTableFromArquivoTexto(string arquivo, DataTable tabela, Delegate AtualizarStatus, params object[] args)
        {
            string content = getConteudoArquivoTexto(arquivo);

            if (content.Trim().Equals("")) return;

            string path = QUEUE_DIRECTORY + "\\" + ARQUIVO_TEXTO + "_" + arquivo + ".txt";
            tabela.TableName = arquivo;
            tabela.ReadXml(path);
        }

        public bool deletaArquivoTexto(string arquivo, string arquivoTexto = null)
        {
            string path = QUEUE_DIRECTORY + "\\" + (arquivoTexto == null ? ARQUIVO_TEXTO : arquivoTexto) + "_" + arquivo + ".txt";
            if (!File.Exists(path))
                return true;
            try
            {
                File.Delete(path);
                return true;
            }
            catch
            {
                Thread.Sleep(1000);
                try
                {
                    File.Delete(path);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public void initEmptyDictionary(Dictionary<string, string> dicionarioArquivoTexto)
        {
            if (dicionarioArquivoTexto == null)
                dicionarioArquivoTexto = new Dictionary<string, string>();
            else
                dicionarioArquivoTexto.Clear();

            dicionarioArquivoTexto.Add("lastCNPJ", "0");
            dicionarioArquivoTexto.Add("lastNota", "0");
            dicionarioArquivoTexto.Add("lastPagina", "1");
            dicionarioArquivoTexto.Add("lastSerie", "0");
            dicionarioArquivoTexto.Add("baixouNotas", "false");
            dicionarioArquivoTexto.Add("lastLoteProcessamento", "0");
            dicionarioArquivoTexto.Add("lastNotaProcessamento", "0");
            dicionarioArquivoTexto.Add("lastNotaInserida", "0");
            dicionarioArquivoTexto.Add("nmArquivo", "");
            dicionarioArquivoTexto.Add("solicitouNotas", "false");
        }

        public Dictionary<string, string> GetDicionarioArquivoTexto()
        {
            #region LÊ ARQUIVO TEXTO DE FLAGS
            string flagsArquivoTexto = getConteudoArquivoTexto("flags");
            Dictionary<string, string> dicionarioArquivoTexto = new Dictionary<string, string>();

            if (flagsArquivoTexto.Trim().Equals(""))
            {
                initEmptyDictionary(dicionarioArquivoTexto);
            }
            else
            {
                try
                {
                    dicionarioArquivoTexto = flagsArquivoTexto.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                                                             .Select(part => part.Split('='))
                                                             .ToDictionary(split => split[0].TrimEnd(), split => split[1].TrimStart());
                }
                catch
                {
                    initEmptyDictionary(dicionarioArquivoTexto);
                    // Deleta arquivos...
                    deletaArquivoTexto("flags");
                    deletaArquivoTexto("parcelasAuditoria");
                    deletaArquivoTexto("ajustesAuditoria");
                    foreach (string arquivo in ARQUIVOS)
                    {
                        Thread.Sleep(1);
                        deletaArquivoTexto(arquivo);
                    }
                }
            }
            #endregion
            return dicionarioArquivoTexto;
        }

        #endregion       

        #region ProgressBar
        public void Processo()
        {
            this.Invoke(new MethodInvoker(delegate ()
            {
                progressBar1.Maximum = total;
                progressBar1.Value = atual;
            }));
        }
        #endregion

        #region Carregar
        public void CarregarProgressBar()
        {
            System.Threading.ThreadStart ts = new System.Threading.ThreadStart(Processo);
            System.Threading.Thread t = new System.Threading.Thread(ts);
            t.IsBackground = true;
            t.Start();
        }
        #endregion

        #region GetCaminho
        public string GetCaminho()
        {
            return Path.Combine(QUEUE_DIRECTORY, lbNomeProcesso.Text, @"Arquivos");
        }
        #endregion

        #region GetCaminhoSoDownload
        public string GetCaminhoSoDownload()
        {
            string caminho = AppDomain.CurrentDomain.BaseDirectory + @"Arquivos\" + DateTime.Now.ToString("yyyyMMdd");

            if (!Directory.Exists(caminho))
            {
                try
                {
                    Directory.CreateDirectory(caminho);
                }
                catch { }
            }

            return caminho;
        }
        #endregion

        #region ApagarArquivos
        public void RemoveArquivos(string pasta)
        {

            if (Directory.Exists(pasta))
            {
                DirectoryInfo dir = new DirectoryInfo(pasta);
                Thread.Sleep(1);
                foreach (FileInfo fi in dir.GetFiles())
                {
                    try
                    {
                        fi.Delete();
                    }
                    catch { }
                }

            }
        }
        #endregion

        #region RemoverCertificado
        private void RemoverCertificado()
        {
            try
            {
                List<string> certificados = getListArquivos(PATH_CERTIFICADO, "*.pfx");

                foreach (string certificado in certificados)
                {
                    X509Certificate2 cert = new X509Certificate2(Path.Combine(PATH_CERTIFICADO, certificado), certificado.Split('-')[1], X509KeyStorageFlags.DefaultKeySet);
                    X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                    store.Open(OpenFlags.ReadWrite);
                    store.Remove(cert);
                    store.Close();
                }
            }
            catch { }
        }
        public List<string> getListArquivos(string caminho, string mascara)
        {
            //string mascara = "*.xml";
            // Le todos os arquivos de um tipo no diretorio
            DirectoryInfo arquivos = new DirectoryInfo(caminho);
            // Pega todas as imagens .gif
            System.IO.FileInfo[] fileNames = arquivos.GetFiles(mascara);
            List<string> listName = new List<string>();
            Thread.Sleep(1);
            foreach (System.IO.FileInfo fi in fileNames)
            {
                listName.Add(fi.Name);
            }
            return listName;
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                timerFechar.Stop();
                timerFechar.Enabled = false;
                timerFechar.Interval = 3600000;
                timerFechar.Enabled = true;
                timerFechar.Tick += new EventHandler(timerFechar_Tick);
                timerFechar.Start();
            }
            catch { }
            ConfigureCEF();
            LoadBrowser();
            path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory.ToString());
            browser = new BrowserChrome(webBrowserI);
            Thread.Sleep(10);
            try
            {
                Configuracao();
            }
            catch
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    this.Close();
                }));
            }

            if (!Program.PROGRAMA_VAI_FECHAR)
                Carregar();
        }
        #region Apagar Diretórios
        public void DeleteDirectory()
        {
            foreach (string path in ListDirectory)
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Delete(path, true);
                    }
                    catch { }
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            //try
            //{
            //    File.Delete(AppDomain.CurrentDomain.BaseDirectory + @"\Conf.txt");
            //}
            //catch { }
            this.Dispose();
        }

        private void timerFechar_Tick(object sender, EventArgs e)
        {

            #region Atualiza status Log Detalhe
            try
            {

                using (BancoDados _dbAtos = new BancoDados(true))
                {

                    _dbAtos.ExecuteScript("nfce.tbNotaFiscalLogDetalhe", (int?)null,
                            @"UPDATE nfce.tbNotaFiscalLogDetalhe SET dtExecucaoFim =getdate(),flSucesso = 0,dsMensagem = 'O programa travou e precisou ser fechado'
                                          WHERE idNotaFiscalLogDetalhe = " + idNotaFiscalLogDetalhe);

                    _dbAtos.ExecuteScript("nfce.tbNotaFiscalLog", (int?)null,
                                        @"UPDATE nfce.tbNotaFiscalLog SET dtExecucao = DATEADD(MINUTE, 20, getDate())
                                            WHERE idNotaFiscalLog = " + NotaFiscalLog.idNotaFiscalLog);
                }
            }
            catch { }
            #endregion
            try
            {
                Program.FinishApp();
                timerFechar.Stop();
                timerFechar.Enabled = false;
                return;
            }
            catch { }
        }

        #region LoadHtmlLoop
        public HtmlDocument LoadHtmlLoop(string xpath = null)
        {
            HtmlDocument document = new HtmlDocument();
            for (int i = 0; i < 30; i++)
            {
                Thread.Sleep(2000);
                document.LoadHtml(browser.PageSourceSync(null, 10));
                if (document != null)
                {
                    if (xpath == null)
                        break;
                    else if (document.DocumentNode.SelectNodes(xpath) != null)
                        break;
                }       
            }

            if (document == null || (xpath != null && document.DocumentNode.SelectNodes(xpath) == null))     
                throw new Exception("Document não foi carregado mesmo após loop");

            return document;
        }
        #endregion
    }
}
