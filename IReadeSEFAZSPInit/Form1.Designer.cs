﻿namespace IReadeSEFAZAMInit
{
    partial class frmIReaderSEFAZAMInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIReaderSEFAZAMInit));
            this.timerInit = new System.Windows.Forms.Timer(this.components);
            this.cbProcessar = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // timerInit
            // 
            this.timerInit.Tick += new System.EventHandler(this.timerInit_Tick);
            // 
            // cbProcessar
            // 
            this.cbProcessar.AutoSize = true;
            this.cbProcessar.Location = new System.Drawing.Point(92, 47);
            this.cbProcessar.Name = "cbProcessar";
            this.cbProcessar.Size = new System.Drawing.Size(73, 17);
            this.cbProcessar.TabIndex = 0;
            this.cbProcessar.Text = "Processar";
            this.cbProcessar.UseVisualStyleBackColor = true;
            this.cbProcessar.CheckedChanged += new System.EventHandler(this.cbProcessar_CheckedChanged);
            // 
            // frmIReaderSEFAZAMInit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 117);
            this.Controls.Add(this.cbProcessar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmIReaderSEFAZAMInit";
            this.Text = "IReader SEFAZAM Init";
            this.Load += new System.EventHandler(this.frmIReaderSEFAZAMInit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerInit;
        private System.Windows.Forms.CheckBox cbProcessar;
    }
}

