﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IReadeSEFAZAMInit
{
    public partial class frmIReaderSEFAZAMInit : Form
    {

        private string FILE_IREADER = AppDomain.CurrentDomain.BaseDirectory + @"IReaderSEFAZAM.exe";
        private string PATH_CASH = AppDomain.CurrentDomain.BaseDirectory + @"GPUCache";
        private DateTime? dtInicioExecucao = null;
        private Semaphore semaforo = new Semaphore(1, 1);
        private bool EmExecucao = false;
        public frmIReaderSEFAZAMInit()
        {
            InitializeComponent();
        }

        private void cbProcessar_CheckedChanged(object sender, EventArgs e)
        {



            timerInit.Stop();
            timerInit.Enabled = false;

            if (cbProcessar.Checked)
            {
                timerInit.Interval = 10000;
                timerInit.Enabled = true;
                timerInit.Start();
            }
        }

        private void IniciarIReaderAM()
        {

            semaforo.WaitOne();
            EmExecucao = true;
            this.Invoke(new MethodInvoker(delegate ()
            {
                cbProcessar.Enabled = false;
            }));
            try
            {

                try
                {
                    if (Directory.Exists(PATH_CASH))
                    {
                        Directory.Delete(PATH_CASH, true);
                    }
                }
                catch { }




                var processos = Process.GetProcessesByName("IReaderSEFAZAM");

                if (processos.Count() < 1)
                {
                    try
                    {
                        X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                        store.Open(OpenFlags.ReadWrite);
                        X509Certificate2Collection certificadosMaquina = store.Certificates;
                        //Removendo os certificados da Máquina para deixar somente o que vai ser usado
                        store.RemoveRange(certificadosMaquina);
                        store.Close();
                    }
                    catch { }
                    Process.Start(FILE_IREADER);
                    dtInicioExecucao = DateTime.Now;
                }
                else if (dtInicioExecucao != null)
                {
                    try
                    {
                        if (DateTime.Now.Date == ((DateTime)dtInicioExecucao).Date && (DateTime.Now.Hour - ((DateTime)dtInicioExecucao).Hour) > 1)
                            processos[0].Kill();
                        else if (DateTime.Now.Date != ((DateTime)dtInicioExecucao).Date)
                        {
                            dtInicioExecucao = DateTime.Now;
                        }
                    }
                    catch { }
                }
                else if (dtInicioExecucao == null)
                {
                    dtInicioExecucao = DateTime.Now;
                }
            }
            catch
            {

            }
            finally
            {

                this.Invoke(new MethodInvoker(delegate ()
                {
                    timerInit.Interval = 60000;
                    timerInit.Enabled = true;
                    timerInit.Start();
                    cbProcessar.Enabled = true;
                }));

                EmExecucao = false;
                semaforo.Release(1);
            }
        }

        private void timerInit_Tick(object sender, EventArgs e)
        {
            if (EmExecucao)
                return;

            timerInit.Stop();
            timerInit.Enabled = false;

            Thread newThreadRecepcao = new Thread(() => IniciarIReaderAM());
            newThreadRecepcao.Name = "AtosCapital - Iniciar IReaderSEFAZAM";
            newThreadRecepcao.Start();

        }

        private void frmIReaderSEFAZAMInit_Load(object sender, EventArgs e)
        {
            cbProcessar.Checked = true;
        }
    }
}
