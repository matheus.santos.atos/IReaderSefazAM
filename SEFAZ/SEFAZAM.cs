﻿using CefSharp.WinForms;
using CefSharp;
using IReaderSEFAZAM.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using HtmlAgilityPack;
using AtosCapital;
using System.Data.SqlClient;
using Ionic.Zip;
using IReaderSEFAZAM.Properties;

namespace IReaderSEFAZAM.SEFAZ
{
    public class SEFAZAM
    {
        private BrowserChrome browser;
        private ChromiumWebBrowser webBrowserI;
        private frmIReaderSEFAZ frm;
        private string PATH_CERTIFICADO;
        private tbNotaFiscalLog NotaFiscalLog;

        public SEFAZAM(frmIReaderSEFAZ frm, string PATH_CERTIFICADO, BrowserChrome browser, ChromiumWebBrowser webBrowserI, tbNotaFiscalLog NotaFiscalLog)
        {
            this.frm = frm;
            this.PATH_CERTIFICADO = PATH_CERTIFICADO;
            this.browser = browser;
            this.webBrowserI = webBrowserI;
            this.NotaFiscalLog = NotaFiscalLog;
        }

        public string AdicionarData(string data)
        {
            // data disposta em dd/mm/yyyy .ToString("dd/MM/yyyy")
            string newData = string.Empty;
            if (!(string.IsNullOrEmpty(data)))
            {
                DateTime parseData = DateTime.Parse(data);
                parseData = parseData.AddDays(1);
                newData = parseData.ToString("dd/MM/yyyy");
                return newData;
            }
            else
                return null;
        }

        #region LerSefazAM
        public void LerSefazAM()
        {
            string dtInicial = "";
            string dtfinal = "";
            string certificadoSefazAM = "";

            try
            {
                try
                {
                    frm.setAtividade("INICIO...");
                    dtInicial = frm.getdtInicial();
                    dtfinal = AdicionarData(frm.getdtFinal());
                    certificadoSefazAM = frm.getlbConnectString();
                    frm.setAtividade("Configurando certificado Digital...");

                }
                catch (Exception ex)
                {
                    try
                    {
                        using (BancoDados _dbAtos = new BancoDados(true))
                        {
                            _dbAtos.ExecuteScript("nfce.tbNotaFiscalLog", (int?)null,
                                                @"UPDATE nfce.tbNotaFiscalLog SET dtExecucao = DATEADD(MINUTE, 20, getDate())
                                            WHERE idNotaFiscalLog = " + NotaFiscalLog.idNotaFiscalLog);
                        }
                    }
                    catch { }
                    #endregion
                    throw new Exception("Falha ao obter os valores iniciais para processamento " + ex.Message);
                }

                X509Certificate2 cert = new X509Certificate2(Path.Combine(PATH_CERTIFICADO, certificadoSefazAM), certificadoSefazAM.Split('-')[1], X509KeyStorageFlags.DefaultKeySet);
                X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                DateTime dtAtual = DateTime.Now;
                DateTime dtExpCert = cert.NotAfter;
                int diasVencer = 0;

                diasVencer = (DateTime.Parse(dtExpCert.ToString("dd/MM/yyyy")).Subtract(DateTime.Parse(dtAtual.ToString("dd/MM/yyyy")))).Days;
                if(diasVencer<15)
                if (dtAtual > dtExpCert)
                {

                    try
                    {
                        using (BancoDados _dbAtos = new BancoDados(true))
                        {
                            _dbAtos.ExecuteScript("nfce.tbNotaFiscalLog", (int?)null,
                                                @"UPDATE nfce.tbNotaFiscalLog SET dtExecucao = DATEADD(MINUTE, 20, getDate())
                                            WHERE idNotaFiscalLog = " + NotaFiscalLog.idNotaFiscalLog);
                        }
                    }
                    catch
                    {

                    }
                    finally
                    {
                        throw new Exception("Cerfificado digital expirado em " + dtExpCert.ToString("dd/MM/yyyy"));
                    }
                }


                X509Certificate2Collection certificadosMaquina = new X509Certificate2Collection();
                try
                {
                    store.Open(OpenFlags.ReadWrite);
                    certificadosMaquina = store.Certificates;
                    //Removendo os certificados da Máquina para deixar somente o que vai ser usado
                    store.RemoveRange(certificadosMaquina);

                    store.Add(cert);
                    store.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                Dictionary<string, string> dicionarioArquivoTexto = frm.GetDicionarioArquivoTexto();

                if (!frm.soLerNotas())
                {
                    if (!Convert.ToBoolean(dicionarioArquivoTexto["baixouNotas"]))
                    {
                        int qtTentativasConsultaSite = 0;
                        int count = 0;
                        browser.ClearAllCookies();
                        browser.reloadCEF();
                        string baseUrl = @"https://online.sefaz.am.gov.br/dte/";
                        string append = "";
                        string url = @"https://online.sefaz.am.gov.br/dte/loginSSL.asp";
                        browser = new BrowserChrome(webBrowserI);
                        browser.Navigate(url);
                        HtmlDocument document = new HtmlDocument();

                        try
                        {
                            frm.setAtividade((qtTentativasConsultaSite == 0 ? "" : "[" + (qtTentativasConsultaSite + 1) + "] ") + "Lendo Site da SEFAZ AM... [1] -> [4]");

                            if (!browser.Esperar2("//*[@id='acessar']", null, 15))
                            {
                                if (!browser.Esperar2("//*[@class = 'barraInferior']", null, 15))
                                {
                                    throw new Exception("Não foi possível carregar o objeto acessar");
                                }
                            }
                            else
                            {
                                HtmlNodeCollection labelTemp = null;
                                string idCnpj = null;
                                while (idCnpj == null)
                                {
                                    document = frm.LoadHtmlLoop();
                                    document.LoadHtml(browser.PageSourceSync());
                                    labelTemp = document.DocumentNode.SelectNodes("//td[contains(@class, 'dg_item')]");
                                    foreach (HtmlNode label in labelTemp)
                                    {
                                        switch (count)
                                        {
                                            case 0:
                                                string cnpj = label.InnerText.Replace(".", "").Replace("-", "").Replace("/", "").Replace("\n", "").Replace("\t", "");
                                                if (NotaFiscalLog.nrCNPJ == cnpj)
                                                {
                                                    idCnpj = cnpj;
                                                    count++;
                                                }
                                                break;

                                            case 1:
                                                append = label.InnerHtml;
                                                int pFrom = append.IndexOf("<a class=\"link\" href=\"") + "<a class=\"link\" href=\"".Length;
                                                int pTo = append.LastIndexOf("\">");
                                                append = baseUrl + append.Substring(pFrom, pTo - pFrom);
                                                count++;
                                                break;

                                        }
                                        if (count == 2)
                                            break;

                                    }
                                    if (idCnpj != null)
                                        break;
                                }
                                if (string.IsNullOrEmpty(append))
                                    throw new Exception("Erro com o link gerado pela aplicação, verificar as opções de entrada referentes a este certificado.");
                                browser.Navigate(append);
                            }

                            Thread.Sleep(2000);
                            if (!browser.Esperar2("//*[@class = 'barraInferior']", null, 15))
                                throw new Exception("Não foi possível carregar o objeto barra inferior");
                            browser.ExecuteScriptAsync("document.getElementById('base_areaGrupo3').click()");

                            Thread.Sleep(2000);
                            if (!browser.Esperar2("//*[@id = 'textoItem94']", null, 15))
                                throw new Exception("Não foi possível carregar o objeto  textoitem94");
                            browser.ExecuteScriptAsync("document.getElementById('textoItem94').click()");

                            Thread.Sleep(2000);
                            if (!browser.Esperar2("//*[@name = 'modelo']", null, 15))
                                throw new Exception("Não foi possível carregar o objeto rodape");
                            browser.ExecuteScriptAsync("document.getElementsByName('modelo')[1].checked = true");
                            browser.ExecuteScriptAsync("document.getElementById('datepicker_de').value='" + dtInicial + "'");
                            browser.ExecuteScriptAsync("document.getElementById('datepicker_ate').value='" + dtfinal + "'");
                            browser.ExecuteScriptAsync("document.getElementsByTagName('form')[0].submit()");

                            if (!browser.Esperar2("//*[@value = '   Download   ']", null, 20))
                                throw new Exception("Não foi possível carregar o objeto button download");
                            try
                            {
                                BaixarNotas(document);
                                System.IO.DirectoryInfo di = new DirectoryInfo(frm.GetCaminho());
                                frm.escreveArquivoTexto("flags", dicionarioArquivoTexto);
                                InsereNotas(dicionarioArquivoTexto);

                            }
                            catch (Exception exce)
                            {
                                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\" + frm.getlbNomeProcesso(), exce.Message + "/n" + exce.StackTrace);
                            }
                            finally
                            {
                                System.IO.DirectoryInfo di = new DirectoryInfo(frm.GetCaminho());
                                foreach (FileInfo file in di.GetFiles())
                                {
                                    file.Delete();
                                }
                            }
                            

                        }
                        catch 
                        {

                        }
                    }
                }
            }
            catch
            {

            }

        }
        #region ProcessaNotas
        public void BaixarNotas(HtmlDocument document)
        {
            int tentativas = 0;
            Boolean baixando = true;
            Boolean temProximo = false;
            HtmlNodeCollection labelTemp = null;
            document = frm.LoadHtmlLoop();
            document.LoadHtml(browser.PageSourceSync());
            labelTemp = document.DocumentNode.SelectNodes("//span/a");
            

            if (labelTemp == null)
            {
                labelTemp = document.DocumentNode.SelectNodes("//span/strong");
                if (labelTemp == null)
                {
                    labelTemp = document.DocumentNode.SelectNodes("//table/tbody/tr/th");
                    if (labelTemp != null)
                        return;
                    throw new Exception("Não foi possivel carregar o nó do documento.");
                }
            }
            if(labelTemp.Count() == 1)
            {
                while (baixando)
                {
                    if (tentativas < 5)
                    {
                        frm.setAtividade("Baixando notas");
                        browser.ExecuteScriptAsync("document.getElementById('selectAll').click()");
                        Thread.Sleep(2000);
                        browser.DownloadFile(120, frm.GetCaminho() + "\\", frm.getlbNomeProcesso(), new Action(() => { browser.ExecuteScriptAsync("document.getElementsByTagName('input')[1].click()"); }));
                        baixando = DownloadExtrairNotas(tentativas);
                    }
                    else
                    {
                        throw new Exception("5 tentativas sem sucesso de download. Abortando");
                    }
                }
            }
            else
            {

                frm.setAtividade("Baixando notas");
                temProximo = true;
                while(temProximo)
                {
                    baixando = true;
                    while (baixando)
                    {
                        if (tentativas < 5)
                        {
                            Thread.Sleep(1000);
                            browser.ExecuteScriptAsync("document.getElementById('selectAll').click();");
                            Thread.Sleep(1000);
                            browser.DownloadFile(60, frm.GetCaminho() + "\\", frm.getlbNomeProcesso(), new Action(() => { browser.ExecuteScriptAsync("document.getElementsByTagName('input')[1].click()"); }));
                            Thread.Sleep(100);
                            baixando = !DownloadExtrairNotas(tentativas);
                        }
                        else
                        {
                            throw new Exception("5 tentativas sem sucesso de download. Abortando");
                        }
                    }
                    HtmlNode element = null;
                    string _tmpUrl = "";
                    try
                    {
                        element = document.DocumentNode.SelectNodes("//span/a[text() = 'Próximo']")[0];
                        Thread.Sleep(1000);
                        if (element == null)
                        {
                            temProximo = false;
                            return;
                        }
                        _tmpUrl = element.Attributes["href"].Value;
                        browser.ExecuteScriptAsync(_tmpUrl);
                        Thread.Sleep(2000);
                        document.LoadHtml(browser.PageSourceSync());
                    }
                    catch
                    {
                        temProximo = false;
                    }
                    frm.setAtividade("Baixando notas");
                }

            }

        }

        public Boolean DownloadExtrairNotas(int tentativas)
        {
            //validar download do arquivo
            Boolean baixado = false;
            DirectoryInfo di = new DirectoryInfo(frm.GetCaminho());
            FileInfo[] fileNames = di.GetFiles("*.zip", SearchOption.TopDirectoryOnly);
            if (fileNames.Where(x => x.Name.Contains(frm.getlbNomeProcesso())).Count() >= 1)
            {
                baixado = true;
            }
            if (baixado)
            {
                //Descompacta os arquivos zip baixados
                frm.setAtividade("Descompactando notas do dia ");
                ZipFile zFile;
                foreach (FileInfo file in fileNames)
                {
                    try
                    {
                        string directory = Path.Combine(file.DirectoryName, file.Name);
                        zFile = new ZipFile();
                        zFile = ZipFile.Read(directory);
                        zFile.ExtractAll(frm.GetCaminho() + "\\");
                        zFile.Dispose();
                        File.Delete(directory);
                    }
                    catch
                    {
                        tentativas++;
                        baixado = false;
                        frm.RemoveArquivos(frm.GetCaminho());
                        frm.DeleteDirectory();
                    }

                }
            }
            return baixado;
        }

        public void InsereNotas(Dictionary<string, string> dicionarioArquivoTexto)
        {

            frm.setAtividade("Lendo Arquivos... [3] -> [4]");

            Nota nt = new Nota();
            nt.escreveArquivoTexto += frm.escreveArquivoTexto;
            nt.escreveArquivoTextoLista += frm.escreveArquivoTexto<NotaFiscal>;
            nt.preencheArquivoTextoLista += frm.preencheListFromArquivoTexto<NotaFiscal>;
            List<NotaFiscal> NotasFiscais = nt.LerXML(dicionarioArquivoTexto, frm.getListArquivos(frm.GetCaminho(), "*.xml"), frm.GetCaminho(), frm);

            frm.setAtividade("Armazenando Arquivos... [4] -> [4]");

            string msg = nt.LerArmazenarXML(dicionarioArquivoTexto, NotasFiscais, frm);
            Thread.Sleep(1000);

            using (BancoDados _dbAtos = new BancoDados(true))
            {

                decimal vltotal = NotasFiscais.Sum(tt => tt.vlTotalNFe);
                int qtNota = NotasFiscais.Count();

                var relatorio = NotasFiscais.GroupBy(tt => tt.dtEmissao.Date).
                            Select(g => new { dtEmissao = g.Key, vlTotal = NotasFiscais.Where(tt => tt.dtEmissao.Date == g.Key).Sum(tt => tt.vlTotalNFe), qtNotas = NotasFiscais.Where(tt => tt.dtEmissao.Date == g.Key).Count() }).ToList();

                string dsDetalhe = "Consulta realizada com sucesso: ";


                if (relatorio == null || relatorio.Count == 0)
                    dsDetalhe += "[vlTotal = R$0.00, dtEmissao: " + NotaFiscalLog.dtInicio.ToString("dd/MM/yyyy") + ", cnpj " + NotaFiscalLog.nrCNPJ + "] ";
                else
                    dsDetalhe += msg;
                if (dsDetalhe.Length > 1000)
                    dsDetalhe = dsDetalhe.Substring(0, 1000).Trim();

                #region Atualiza status Log Detalhe
                try
                {
                    _dbAtos.ExecuteScript("nfce.tbNotaFiscalLogDetalhe", (int?)null,
                            @"UPDATE nfce.tbNotaFiscalLogDetalhe SET dtExecucaoFim =getdate(),flSucesso = 1,dsMensagem = '" + dsDetalhe + @"'
                                          WHERE idNotaFiscalLogDetalhe = " + frm.idNotaFiscalLogDetalhe);
                }
                catch (Exception ex)
                {
                    //throw new Exception("Falha ao atualizar o status em security.tbBancoDadosDistribuidoMigracaoLog");
                }
                #endregion

                #region Atualiza status Log
                try
                {
                    List<SqlParameter> parametros = new List<SqlParameter>()
                            {
                            new SqlParameter("@vlTotalNotas",vltotal),
                            new SqlParameter("@qtInsercoes",qtNota),
                            };
                    _dbAtos.ExecuteScript("nfce.tbNotaFiscalLog", (int?)null,
                            @"UPDATE nfce.tbNotaFiscalLog SET dtFinalizado =getdate(),vlTotalNotas = @vlTotalNotas,qtInsercoes= @qtInsercoes
                                          WHERE idNotaFiscalLog = " + NotaFiscalLog.idNotaFiscalLog, parametros);
                }
                catch (Exception ex)
                {
                    throw new Exception("Falha ao atualizar o status em security.tbBancoDadosDistribuidoMigracao");
                }
                #endregion
            }

            frm.ListDirectory = new List<string> { AppDomain.CurrentDomain.BaseDirectory + frm.getlbNomeProcesso() + @"\Arquivos" };
            frm.ListDirectory.Add(AppDomain.CurrentDomain.BaseDirectory + frm.getlbNomeProcesso());
            frm.RemoveArquivos(frm.GetCaminho());
            frm.DeleteDirectory();
            Directory.Delete(AppDomain.CurrentDomain.BaseDirectory + frm.getlbNomeProcesso(), true);

        }

        #endregion



    }
}
