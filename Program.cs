﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IReaderSEFAZAM
{
    static class Program
    {
        public static bool PROGRAMA_FALHOU = false;
        private static bool PROGRAMA_VAI_REINICIAR = false;
        public static bool PROGRAMA_VAI_FECHAR = false;
        

        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string processName = Process.GetCurrentProcess().ProcessName;
            Process[] processlist = Process.GetProcessesByName(processName);
            if (processlist.Length > 1)
            {
                //MessageBox.Show("Não é possível abrir mais que 1 instâncias do iReader SP!");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            Application.Run(new frmIReaderSEFAZ());
        }

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                FinalizaComErro(ex == null ? "" : (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException == null ? ex.InnerException.Message : ex.InnerException.InnerException.Message) + (ex.StackTrace == null ? "" : "<br><br>" + ex.StackTrace));
            }
            catch (Exception exc)
            {
                FinalizaComErro(exc == null || exc.Message == null ? "UnhadledExceptionHandler" : exc.Message + (exc.StackTrace == null ? "" : "<br><br>" + exc.StackTrace));
            }
        }


        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            FinalizaComErro(e.Exception == null ? "ApplicationThreadException" : (e.Exception.InnerException == null ? e.Exception.Message : e.Exception.InnerException.InnerException == null ? e.Exception.InnerException.Message : e.Exception.InnerException.InnerException.Message) + (e.Exception.StackTrace == null ? "" : "<br><br>" + e.Exception.StackTrace));
        }

        private static void FinalizaComErro(string erroMessage)
        {
            if (PROGRAMA_VAI_FECHAR) return;

            MessageBox.Show(erroMessage);

            // Mandar push para o catálogo

            FinishApp();
        }

        public static void FinishApp()
        {
            if (PROGRAMA_VAI_FECHAR || PROGRAMA_VAI_REINICIAR) return;

            PROGRAMA_VAI_FECHAR = true;
            Application.Exit();
        }

        public static void RestartApp()
        {
            if (PROGRAMA_VAI_FECHAR || PROGRAMA_VAI_REINICIAR) return;
           
            PROGRAMA_VAI_REINICIAR = true;
            PROGRAMA_FALHOU = true;

            Application.Restart();
        }
    }
}
